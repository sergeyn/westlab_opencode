/*
 * utils.h
 *
 *  Created on: Mar 22, 2012
 *      Author: sergeyn
 */

#ifndef UTILS_H_
#define UTILS_H_
#include <limits>
#include "globals.h"
#include <algorithm>

#define PARANOIC_DEL(p) if(p) delete p; p=0;
#define PARANOIC_DEL_ARR(p) if(p) delete[] p; p=0;
#define CALC_SCORE(freq,lendid,pre) pre * (float)(freq) / ((float)(freq + 2 * ( 0.25 + 0.75 * (float)(lendid)/(float)(130) ) ) )

float avgDocumentLength();
std::vector<int> loadEntireIndexListLengths();
std::vector<int> loadLoadedIndexListLengths(termsMap& lex);
std::vector<int> loadQueryLogListLengths(termsMap& lex);
void printListLengthDistribution(std::vector<int>& listLengths);
void spaceComparison(std::vector<int>& listLengths);


// Knuth trick for comparing floating numbers
inline bool FloatEquality( float a, float b);
inline bool FloatComparisonPrivate( float a, float b);

// Arguments a,b and returns a) 0 in case of equality, b) 1 in case a > b, and c) -1 in case a < b
inline int Fcompare( float a, float b);

inline uint32_t intlog2(const uint32_t x);

inline unsigned int NearestPowerOf2( unsigned int x );

template <typename T> //stream vectors
std::ostream& operator<< (std::ostream& s, const std::vector<T>& t);


// Find live areas, given lps and threshold output a vector of liveareas in format <live_area1 = [start,end]> , ... <live_areak = [start,end]>
//inline void find_live_areas_with_hash(lptrArray& lps, const float threshold, std::vector <int>& live_areas) {
//}

std::string getTimeString();

template <typename T, unsigned k>
struct ktuple {
	T tuple[k];
	ktuple(){}
	ktuple(T arr[k]) { for(unsigned i=0; i<k; ++i) tuple[i]=arr[i];}
	inline const T& operator[](unsigned i) const { return tuple[i];}
	inline T& operator[](unsigned i) { return tuple[i];}
};

template<class T>
class PriorityArray{
	const unsigned size;
	std::vector<T> minData;

public:
	PriorityArray(const size_t sz=CONSTS::NUMERIC::TOPK);
	inline const std::vector<T>& getV() const;
	inline void putInArray(T* ptr);
	inline const T& head() const;
	inline void sortData();
	inline bool push(const T& val);
};

template <typename T>
inline T max3(const T& a, const T& b, const T& c);

template <typename T>
inline T min3(const T& a, const T& b, const T& c);

//TODO: rewrite all this parsing nightmare
//return -1 means finish, 1 means valid
int readline(FILE* fd, char* line);

inline unsigned offsetInAlignedForSSE(void* ptr);

inline bool isAlignedForSSE(void* ptr);

unsigned countSetBits(void* ptr, unsigned sizeInBytes);

extern unsigned allocatedSum;
inline void NEWMA(void** ptr,const unsigned bytecounts);

#define NEWM new
#define DEL delete[]
#define DELA delete[]

template <typename S=float>
struct QpResult {
	unsigned int did;
	S score;

	inline QpResult<S>& operator=(const QpResult<S>& rhs) {
	        did=rhs.did;
	        score=rhs.score;
        	return *this;
	}

	inline bool operator>(const QpResult<S>& rhs) const { return score>rhs.score; }
	inline bool operator<(const QpResult<S>& rhs) const  { return score<rhs.score; } 

	inline QpResult(unsigned int d=CONSTS::NUMERIC::MAXD+1, S s=0){ did=d; score=s;}
	inline void setR(unsigned int d, S s){ did=d; score=s;}
};

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
template<typename _FIter, typename _Tp>
_FIter gallop_up_lower_bound(_FIter begin, _FIter end, const _Tp& val);

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
//this is not the same convention as std:: version -- we need it for nextGEQing
template<typename _FIter, typename _Tp>
_FIter gallop_down_lower_bound(_FIter begin, _FIter curr, const _Tp& val);


template <typename Iterator>
void dumpToFile(const std::string& path, Iterator start, Iterator end);

#include "utilsIMP.h"

#endif /* UTILS_H_ */
