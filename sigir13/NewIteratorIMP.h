#ifndef NEWITERATORIMP_H_
#define NEWITERATORIMP_H_

#include "pfor.h"
#include "profiling.h"

//=============================================================================================
template<typename It, typename T>
inline void popdown(It start, It end) {
	 T* temp = *(start); 
         ++start;
         while( start != end && temp->did >= ( (*start)->did) )        {
                       *(start-1) = *(start); //this->operator [](j-1) = this->operator [](j); //swap
                        ++start;
          }
	  *(start-1) = temp; //  this->operator [](j - 1) = temp;
}
//=============================================================================================
	template<typename It, typename T>
	inline void sortByDid(It start, It end){ //assumes that derefernecing an iterator with *it gives a T* that has a did member
			std::sort(start, end, [](const T* a, const T* b){ return a->did < b->did; });
}
	template<typename It, typename T> //assumes that derefernecing an iterator with *it gives a T* that has a getMaxScore method
	inline void sortByMaxscore(It start, It end) {
	std::sort(start, end, [](const T* a, const T* b){ return a->getMaxScoreOfList() < b->getMaxScoreOfList(); });
}		
//=============================================================================================
	template<typename It>
	inline did_t getSmallestDid(It start, It end) {
			assert(end-start>0);
		did_t smallest = (*start)->did;
		for(start = start+1; start!=end; ++start)
			if((*start)->did < smallest) smallest = (*start)->did;
		return smallest;
	}
//=============================================================================================
inline unsigned NewIteratorBase::getFreq() 	{
#ifdef USEUNCOMPRESSEDQUANTS
	return rawQuants[compressedBlock*CONSTS::NUMERIC::BS + offsetInPostingSpace];
#endif
	if (fflag == 0)  {
		pack_decode_freqs(freqsBuffer,ptrToFreqs, flags[compressedBlock]>>16);
		fflag = 1;
	}

#ifdef USEQUANTINDEX
	return(freqsBuffer[offsetInPostingSpace]); //this is the actual quantized score
#endif
	return(freqsBuffer[offsetInPostingSpace]+1); //this is the freq, hence the +1
}

//=============================================================================================
inline did_t NewIteratorBase::nextGEQ(const did_t d) {				//changes state. returns did s.t. did>=d
	if(d<did) return did; //if we are already greater or equal -- return

#ifdef USEREALSCORES
	if(useReals) {
		//profilerC::getInstance().stepCounter(enm::NEXTGEQ_CHEAP);
		//if we use realscores and have realdids set
		// check if we need to skip to a new block of size BS
		const unsigned oldPS = compressedBlock;

		//debug
		//COUTL << "---------------------------" <<ENDL;
		//COUTL << "did: " << d << " maxdid: " << maxDidPerBlock[compressedBlock+1] << ENDL;
		while (d > maxDidPerBlock[compressedBlock+1]) ++compressedBlock;
		offsetInPostingSpace += (compressedBlock-oldPS) ? (compressedBlock-oldPS-1)*CONSTS::NUMERIC::BS : 0;

		//debug
		//COUTL << "offsetinposting: " << offsetInPostingSpace << " postingsinwindow: " << postingsInWindow << ENDL;
		while(did<d && offsetInPostingSpace<postingsInWindow) did = realDids[++offsetInPostingSpace];
		//compressedBlock = offsetInPostingSpace/CONSTS::NUMERIC::BS;
		assert(did<CONSTS::NUMERIC::MAXD+128);
		assert(compressedBlock<=paddedLength/CONSTS::NUMERIC::BS);
		//COUTL << "returned did: " << d << ENDL;
		//COUTL << "---------------------------" <<ENDL;
		return did;
	}
#endif

	profilerC::getTheInstance().stepCounter(enm::NEXTGEQ);
	//if no realdids:
	if(d>maxDidPerBlock[compressedBlock+1]) {//can't make it in this compressed block
		skipToDidBlockAndDecode(d);
		offsetInPostingSpace=0;
	}

	while(d>did){
		did += didsBuffer[++offsetInPostingSpace]; //prefix sum
	}
	assert(offsetInPostingSpace<=CONSTS::NUMERIC::BS);
	assert(did<CONSTS::NUMERIC::MAXD+128);
	return did;
}

//=============================================================================================
inline unsigned	NewIteratorBase::getPaddedLength() const { return paddedLength; }
//=============================================================================================
inline unsigned * NewIteratorBase::entireBlockDids(did_t* dids, const unsigned block) const {
	unsigned offset = (sizePerBlock[block]);
	unsigned *ptr = pack_decode_dids(dids, compressedBase + offset, flags[block]&CONSTS::NUMERIC::maskFlagWithOnes, maxDidPerBlock[block]); //decode new block

	//prefix sum dids
	for(unsigned i=1; i<CONSTS::NUMERIC::BS; ++i)
		dids[i]+=dids[i-1]; //dbuf[0] is the real did, not a gap
	
	return ptr;

}
//=============================================================================================
template <int Mode> //mode 0 -- initial build call, mode 1 -- inprocessing
inline void NewIteratorShortScore::entireBlockDidsAndScores(did_t* dids, scoreInt_t* scores, const unsigned block) const { //decode given block and calc sco$
	static unsigned freqs[CONSTS::NUMERIC::BS];
	unsigned *ptr = entireBlockDids(dids,block);
	if(!Mode) { //initial
                pack_decode_freqs(freqs,ptr, flags[block]>>16);
                for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i)
                        scores[i]=freqs[i]; //can be sped up...
	}
	else { //inproc.
		#ifdef USEUNCOMPRESSEDQUANTS		
	 	 	const unsigned blockStart = block*CONSTS::NUMERIC::BS;
	                for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i)
                        	scores[i]=rawQuants[blockStart+i]; //can be sped up...
		#else
			pack_decode_freqs(freqs,ptr, flags[block]>>16);
	                for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i)
        	                scores[i]=freqs[i]; //can be sped up...
		#endif

	}
}
//=============================================================================================
inline void NewIteratorFloatScore::entireBlockDidsAndScores(did_t* dids, score_t* scores, const unsigned block) const { //decode given block and calc scores
	static unsigned freqs[CONSTS::NUMERIC::BS];
	static unsigned lengths[CONSTS::NUMERIC::BS];	
	unsigned *ptr = entireBlockDids(dids,block);
//#ifndef USEUNCOMPRESSEDQUANTS
	pack_decode_freqs(freqs,ptr, flags[block]>>16);
//#endif 
#ifdef USEQUANTINDEX
//	#ifndef USEUNCOMPRESSEDQUANTS
//		//for(unsigned i=0; i<64; ++i) scores[i]=freqs[i]*CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE;
	       	dequantLinear(freqs, scores, CONSTS::NUMERIC::BS, CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE);
//	#else
//		dequantLinear(rawQuants+(block*CONSTS::NUMERIC::BS), scores, CONSTS::NUMERIC::BS, CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE);
//	#endif
#else

	for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i) {
		lengths[i]=documentLengthArr[dids[i]]; //build lengths array
	}

	for(unsigned i=0; i<CONSTS::NUMERIC::BS; i+=4) 	{
			__m128 freq = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(freqs+i)));
			__m128 len = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(lengths+i)));
			__m128 res = calcScore4(freq,len); //sse version -- knows to add +1 to freqs
			_mm_store_ps(scores+i,res);
	}
#endif
}
//=============================================================================================
inline __m128 NewIteratorFloatScore::calcScore4(__m128 freq, __m128 len) const {
	__m128 v1 = {BM25Element,BM25Element,BM25Element,BM25Element};
	__m128 v2 = {2,2,2,2};
	__m128 v3 = {0.25,0.25,0.25,0.25};
	__m128 v4 = {0.75,0.75,0.75,0.75};
	__m128 v6 = {1.0/130,1.0/130,1.0/130,1.0/130};
	__m128 ones = {1.0,1.0,1.0,1.0};
	freq = _mm_add_ps(freq,ones);
	__m128 result =
	_mm_div_ps(_mm_mul_ps(v1,freq),
			   _mm_add_ps(_mm_mul_ps(_mm_add_ps(_mm_mul_ps(_mm_mul_ps(len,v6),v4),v3),v2),freq));
	return result;
}
//=============================================================================================
inline score_t NewIteratorFloatScore::calcScore(const unsigned freq, const unsigned doclen) const { return CALC_SCORE(freq,doclen, BM25Element); 	}
//=============================================================================================
inline score_t NewIteratorFloatScore::getScore() {  //returns score for current did. this is either real score, result of score calc, or dequantization
#ifdef USEREALSCORES
		if(useReals) return realScores[offsetInPostingSpace];
#endif
#ifdef USEQUANTINDEX
		return getFreq()*CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE;  //if a lookup is used different version needed
#endif
		return calcScore(getFreq(),documentLengthArr[did]);
}
//=============================================================================================
inline scoreInt_t NewIteratorShortScore::getScore(){  //returns score for current did. this is either real score, result of score calc, or dequantization
#ifdef USEREALSCORES
		if(useReals) return realScores[offsetInPostingSpace];
#endif
	return getFreq(); //make sure usequantindex is defined -- otherwise it is going to return +1ed values.
}
//=============================================================================================
// skip blocks until you come to the right one
inline void NewIteratorBase::skipToDidBlockAndDecode(const did_t d) { //populate the buffers with decompressed data of posting block containing d's nextGEQ
	compressedBlock++; //we enter this only when needed
	while (d > maxDidPerBlock[compressedBlock+1]) 		compressedBlock++;

	//no evidence speeding up when using this:
	//compressedBlock = ((gallop_up_lower_bound(maxDidPerBlock+compressedBlock+1,maxDidPerBlock+paddedLength/CONSTS::NUMERIC::BS, d))-maxDidPerBlock)-1;

	offsetFromBase = (sizePerBlock[compressedBlock]);

	ptrToFreqs = pack_decode_dids(didsBuffer, compressedBase + offsetFromBase, flags[compressedBlock]&CONSTS::NUMERIC::maskFlagWithOnes, maxDidPerBlock[compressedBlock]); //decode new block
	offsetInPostingSpace = 0; //point to first element in that block
	did = didsBuffer[0]; //get the first did IN THE DECODED block
	fflag = 0;			 //indicating that we didn't decompress the freqs yet
}

#ifdef	USEAUGMENTS
//=============================================================================================
#define DBLOCK ((d-firstDidOfWindow)>>didBlockBits)

//because we could have defined it via Makefile and then redefine...
#ifndef USEPOSTINGS
#define USEPOSTINGS
#endif

#ifdef USEPOSTINGS
#define MTHREE 3 // In case we want to use only LA, hardcode MTHREE to 0
#else
#define MTHREE 0
#endif
#define MBITS	(didBlockBits-MTHREE)
#define DMBLOCK ((d-firstDidOfWindow)>>(didBlockBits-MTHREE))
//=============================================================================================
inline void		NewIteratorAugmentedBase::setPolicy(enm::prprPolicy p) { policy = p; }
//=============================================================================================
inline void 		NewIteratorAugmentedBase::setScratchId(const unsigned id) { scratchPadId = id; }
inline unsigned 	NewIteratorAugmentedBase::getScratchId() const { return scratchPadId; }
//=============================================================================================
inline enm::prprPolicy
					NewIteratorAugmentedBase::getPolicy() const {return policy;}			//get the policy
//=============================================================================================
inline did_t   		NewIteratorAugmentedBase::nextLiveArea(const did_t d) {
	assert(d>=firstDidOfWindow);
	//COUTL << firstDidOfWindow+(getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS) << " " << (getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS) << ENDL; //debug
	return std::min(CONSTS::NUMERIC::MAXD,std::max(d,firstDidOfWindow+(getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS)));
}
//=============================================================================================
inline did_t   		NewIteratorAugmentedBase::nextLivePosting(const did_t d) const {
	return std::min(CONSTS::NUMERIC::MAXD,std::max(d,firstDidOfWindow+(getNextSetBit(postingBitSet,DMBLOCK)<<MBITS)));
}
//=============================================================================================
inline score_t 		NewIteratorAugmented::getMaxScoreOfBlock(const did_t d) const {
	return blockMaxes[(d-firstDidOfWindow)>>didBlockBits];
}
//=============================================================================================
inline score_t 		NewIteratorAugmented::getMaxScoreOfList() const {
	return maxScoreOfList;
}
//=============================================================================================
inline scoreInt_t 	NewIteratorIntAugmented::getMaxScoreOfBlock(const did_t d) const {
	return blockMaxes[(d-firstDidOfWindow)>>didBlockBits];
}
//=============================================================================================
inline scoreInt_t 	NewIteratorIntAugmented::getMaxScoreOfList() const {
	return maxScoreOfList;
}
//=============================================================================================
inline did_t NewIteratorAugmentedBase::getFirstDidOfNextBlock( const did_t docID) const {
	return ( ( (docID>>didBlockBits) + 1) << didBlockBits);
}
//=============================================================================================
inline did_t NewIteratorAugmentedBase::getFirstDidinWindow() const {
	return firstDidOfWindow;
}
//=============================================================================================
inline unsigned	NewIteratorAugmentedBase::getNextSetBit(const unsigned* ptrBS, const unsigned index) { //assume last is always set!
	//note! if usetype is long (64 bits) you must use ctzl, if smaller, use ctz!
	typedef unsigned long usetype;
	const unsigned logBitsInType = 6;
	const unsigned bitsInType =  8*sizeof(usetype);
	assert(1<<logBitsInType == 8*sizeof(usetype));

	const unsigned packIndex = index>>logBitsInType;
	const usetype *ptr =  ((usetype *)ptrBS);


	unsigned offsetInPacked = index%bitsInType;
	if(ptr[packIndex]>>offsetInPacked)
		return (packIndex*bitsInType)+offsetInPacked+__builtin_ctzl(ptr[packIndex]>>offsetInPacked);

	unsigned int i=packIndex+1;
	for(; ptr[i]==0; ++i); //skip to next live pack

	unsigned packOffset = __builtin_ctzl(ptr[i]);
	return (i<<logBitsInType) + packOffset;
}
//=============================================================================================

#endif
#endif /* NEWITERATORIMP_H_ */
