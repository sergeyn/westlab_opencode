#include <cstring>
#include "NewIterator.h"
#include "profiling.h"

using namespace enm;

unsigned* 	NewIteratorBase::documentLengthArr;
unsigned  	NewIteratorBase::instancesCounter(0);


#ifdef	USEAUGMENTS
unsigned* 	NewIteratorAugmentedBase::fakePostingBitSet(nullptr);
score_t*   	NewIteratorAugmented::scoresBuffer;
unsigned  	NewIteratorAugmentedBase::didBlockBits(6);
unsigned  	NewIteratorAugmentedBase::currentWindow(0);
did_t*	  	NewIteratorAugmentedBase::scratchPadDids[16];
score_t*  	NewIteratorAugmented::scratchPadScores[16];
unsigned* 	NewIteratorAugmentedBase::scratchPadPBS[16];
score_t*  	NewIteratorAugmented::scratchPadBMs[16];
score_t*	NewIteratorAugmented::blockScores[16];
unchar*		NewIteratorAugmentedBase::bitSetsPBS[16];
unsigned* 	NewIteratorAugmentedBase::scratchPadQBMs[16];
unsigned* 	NewIteratorAugmentedBase::liveDeadArea(nullptr);
unsigned  	NewIteratorAugmentedBase::firstDidOfWindow(0);
NewIteratorAugmented NewIteratorAugmented::scratchPads[16];


scoreInt_t*   	NewIteratorIntAugmented::scoresBuffer;
scoreInt_t*  	NewIteratorIntAugmented::scratchPadScores[16];
scoreInt_t*  	NewIteratorIntAugmented::scratchPadBMs[16];
scoreInt_t*	NewIteratorIntAugmented::blockScores[16];
NewIteratorIntAugmented NewIteratorIntAugmented::scratchPads[16];
#endif

//=============================================================================================
NewIteratorBase::NewIteratorBase(){
	if( ++NewIteratorBase::instancesCounter > 32) FATAL("Why do you need so many iterators?");
}
//=============================================================================================
void NewIteratorBase::setDocLenPtr(unsigned* ptr) {
	NewIteratorBase::documentLengthArr = ptr;
}
//=============================================================================================
void NewIteratorBase::setUseReals(bool use) {
	useReals = use;
}
//=============================================================================================
void NewIteratorAugmented::initBM25Float(const unsigned unpaddedLen) {
	BM25Element = (float)3 * (float)(  log( 1 + (float)(CONSTS::NUMERIC::MAXD - unpaddedLen + 0.5)/(float)( unpaddedLen + 0.5))/log(2));
	#ifdef USEREALSCORES
	realScores = nullptr;
	#endif
}
//=============================================================================================
//open list sets the compressed data related pointers
void NewIteratorBase::injectCompressedState(const unsigned unpaddedLen, unsigned *data, unsigned *sizes, unsigned* maxes, unsigned* flgs) {

	paddedLength = (0 == unpaddedLen%CONSTS::NUMERIC::BS)  ? unpaddedLen : unpaddedLen + CONSTS::NUMERIC::BS - (unpaddedLen%CONSTS::NUMERIC::BS);	//number of postings in lists padded to be BS aligned

	assert(paddedLength && paddedLength<CONSTS::NUMERIC::MAXD && paddedLength%CONSTS::NUMERIC::BS == 0);
	assert(data && sizes && maxes && flgs);

	compressedBase = data;	// pfor compressed chunks
	maxDidPerBlock = maxes;	// arr of max values for each chunk
	sizePerBlock = sizes;	// arr of sizes for each chunk
	flags = flgs;			// arr of pfor flags for each chunk -- Shuai!!!
	resetPostingIterator();

#ifdef USEREALSCORES
	realDids = nullptr;
#endif
}
//=============================================================================================
void NewIteratorBase::resetPostingIterator() { //reset the list after traversal for new round
	offsetFromBase = compressedBlock = offsetInPostingSpace = fflag = 0;
	//decompress first block
	ptrToFreqs = pack_decode_dids(didsBuffer, compressedBase, flags[0]&CONSTS::NUMERIC::maskFlagWithOnes, 0); //decode new block
	did = didsBuffer[0];
}
//=============================================================================================

#ifdef	USEAUGMENTS
PreprocessingData::PreprocessingData() :
							blockMaxes(nullptr), quantBlockMaxes(nullptr),
							postingBitSet(nullptr), quantile(0),nonZeroBlockMaxes(nullptr),
							compressedZeros(nullptr),flagsForBMs(nullptr),sizesForBMs(nullptr),
							compressedBitset(nullptr), flagsForPBS(nullptr), sizesForPBS(nullptr)
{}

void dequantLinear(const unsigned char* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile);
void dequantLinear(const unsigned* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile);
//=============================================================================================
NewIteratorAugmentedBase::NewIteratorAugmentedBase(){}
NewIteratorAugmented::NewIteratorAugmented()  {}
NewIteratorIntAugmented::NewIteratorIntAugmented()  {}
//=============================================================================================
void NewIteratorIntAugmented::initListFromCacheIntPart(PreprocessingData& initData) {
	blockMaxesBase = initData.globalQuantBlMaxes;
        #ifdef USEUNCOMPRESSEDQUANTS
                rawQuants = initData.rawQuants;
        #endif

}
//=============================================================================================
void NewIteratorAugmented::initListFromCacheFloatPart(PreprocessingData& initData) {
	blockMaxesBase = initData.blockMaxes;
	quantileForBM = initData.quantile;
        #ifdef USEUNCOMPRESSEDQUANTS
                rawQuants = initData.rawQuants;
        #endif
	#ifdef DEBUG
		res = initData;
	#endif
}
//=============================================================================================
void NewIteratorAugmentedBase::initListFromCache(PreprocessingData& initData) {
	if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC)
		bmQorC = initData.compressedZeros;
	else
		bmQorC = reinterpret_cast<unsigned*>(initData.quantBlockMaxes);
	flagsBM=initData.flagsForBMs;
	unsigned tmp[4] = {0,0,0,0};
	stateForBMQC = ktuple<unsigned,4>(tmp);
	nonZeroesBase = initData.nonZeroBlockMaxes;
	postingBitSetBase = initData.postingBitSet;
	sizesCompBMs = initData.sizesForBMs;
	#ifdef DEBUG
	if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC)
		for(unsigned i=1; i<64; ++i) if(sizesCompBMs[i]<=sizesCompBMs[i-1]) FATAL("!?");
	#endif
	currentWindow = 0;
	//did=0;
}
//=============================================================================================
void NewIteratorAugmentedBase::setBits(const unsigned bits) {	 //set the bits of did space block
	didBlockBits = bits;
}
//=============================================================================================
static inline void setBit(unsigned char* ptr, const unsigned bitNumber) {
	ptr[bitNumber>>3] |= (1<<(bitNumber&7)); //turn on the one bit to set in the right byte
}
//=============================================================================================
void NewIteratorAugmentedBase::incCurrentWindow() {
	++currentWindow;
	memset((void*)liveDeadArea,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
}
//=============================================================================================
//todo templated version...
static void buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
	assert(dids && dids[0]>=winStartDid);
	bits -= 3; //this is why it is the M3 version -- every max-block has 8 miniblocks
	unsigned  alignedSize = (sz>>2)<<2;
	unsigned char* packed = (unsigned char*)dest;
	int four[4] = {0,0,0,0};
	unsigned  starts[4] = { winStartDid,winStartDid,winStartDid,winStartDid};
	__m128i startsS = _mm_load_si128( (__m128i*)(starts));
	__m128i shiftbits = {bits,bits};
	for(unsigned i=0; i<alignedSize; i+=4) {
		__m128i a = _mm_sub_epi32(_mm_load_si128( (__m128i*)(dids+i)), startsS); //load 4 dids and subtract the window start from them
		_mm_store_si128((__m128i*)(four), _mm_srl_epi32(a,shiftbits) ); //shift all 4 dids
		setBit(packed,four[0]); setBit(packed,four[1]); setBit(packed,four[2]); setBit(packed,four[3]); //set appropriate bits (for 4 shifted dids)
	}
	//leftovers:
	for(unsigned i=alignedSize; i<sz; ++i)
		setBit(packed, (dids[i]-winStartDid)>>bits);
}
//=============================================================================================
void NewIteratorAugmented::handleAWindowOfOnTheFly(const did_t firstDidOfWindow, const did_t nextWindowsFirst) {
	//decompress and calc scores
	unsigned* blockI; //find starting block of a window

	unsigned safeMaxDidPerBlock = (paddedLength/CONSTS::NUMERIC::BS);// != 1 ? (paddedLength/CONSTS::NUMERIC::BS)-1 : 1;
	if(firstDidOfWindow>maxDidPerBlock[safeMaxDidPerBlock]) //oldcode(paddedLength/CONSTS::NUMERIC::BS)-1] ) //can't make it
	{
		COUTV(5) << scratchPadId << " can't make it: " << firstDidOfWindow << ">" << maxDidPerBlock[safeMaxDidPerBlock] /*(paddedLength/CONSTS::NUMERIC::BS)-1]*/ << ENDL;
		postingsInWindow = 0;
		blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
		memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 2); //we need 4 times the space for block maxes
		//we are constructing on the fly -- that means no fake pbs
		postingBitSet = scratchPadPBS[scratchPadId];
		bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
		memset(postingBitSet,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		postingBitSet[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]=128; //always set the last one...

		while (nextWindowsFirst > maxDidPerBlock[compressedBlock+1]) ++compressedBlock;
		realDids = scratchPadDids[scratchPadId];
		realDids[0] = did = nextWindowsFirst;
		realScores = scratchPadScores[scratchPadId];
		realScores[0] = 0.0f;
		//offsetInPostingSpace = 0;
		return;
	}

	blockI = std::lower_bound(maxDidPerBlock,maxDidPerBlock+1+(paddedLength/CONSTS::NUMERIC::BS),firstDidOfWindow);
	unsigned block = blockI - maxDidPerBlock;

	if(block) --block; //are you sure?
	compressedBlock = block; //this is the first block of a did-s window in comp-postings-space
	assert(block<paddedLength/CONSTS::NUMERIC::BS);
	realDids = scratchPadDids[scratchPadId];
	realScores = scratchPadScores[scratchPadId];
	unsigned postings = 0;
	//special care for first block...
	entireBlockDidsAndScores(didsBuffer,scoresBuffer,block++);
	unsigned i=0;
	while(didsBuffer[i]<firstDidOfWindow) ++i; //skip all those that are not in this window

	assert(i<=CONSTS::NUMERIC::BS); //we mustn't skip more than a block -- if so, our block calc was flawed!
	//we will have written (CONSTS::NUMERIC::BS - i) postings and would like to be aligned to 16 bytes AFTER
	unsigned scoresGapInWin; //for compensation
	for(scoresGapInWin=0; ((CONSTS::NUMERIC::BS - i + scoresGapInWin)*sizeof(score_t))%16; ++scoresGapInWin) {}
	realScores += scoresGapInWin;

	for(; i<CONSTS::NUMERIC::BS; ++i) {
		realScores[postings]=scoresBuffer[i];
		realDids[postings++]=didsBuffer[i];
	}

	//and now all other blocks
	while(realDids[postings-1]<nextWindowsFirst) {
		entireBlockDidsAndScores(realDids+postings,realScores+postings,block++);
		postings+=CONSTS::NUMERIC::BS;
	}
	//refine the last block:
	while(postings>0 && realDids[postings-1]>=nextWindowsFirst) --postings;
	postingsInWindow = postings; //getScores can use this threshold later

	blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
	memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 2); //we need 4 times the space for block maxes
	score_t dummy; //calculate block maxes

	for(size_t i=0; i<postingsInWindow; ++i) {
		const score_t scoreI = realScores[i];
		const did_t did = realDids[i];
		assert(did>=firstDidOfWindow && did<nextWindowsFirst);
		assert(((did-firstDidOfWindow)>>didBlockBits) < CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		score_t* buf[2] = {&dummy,blockMaxes+((did-firstDidOfWindow)>>didBlockBits)};
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}


	//we are constructing on the fly -- that means no fake pbs
	bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
	postingBitSet = scratchPadPBS[scratchPadId];
//TODO: add ifdef usepostings!
#ifdef USEPOSTINGS
	memset(postingBitSet,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	buildPBS_M3(postingBitSet,realDids, firstDidOfWindow, didBlockBits, postingsInWindow);
#endif
	postingBitSet[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]|=128; //always set the last one...

	//if(scratchPadId==1)
	//for(unsigned i=0; i<postings; ++i)	COUT << realDids[i] << " " << realScores[i] << ENDL;
	//	COUTL << scratchPadId << "@" << currentWindow << " "  << realDids[0] << " " << realDids[postings-1] << ENDL;

	did = realDids[0];
	offsetInPostingSpace = 0;
	assert(compressedBlock<paddedLength/CONSTS::NUMERIC::BS);
	//COUTL << NewIteratorAugmented::debugGetBMForList(1,3067)<< ENDL;
}
//=============================================================================================
void NewIteratorIntAugmented::handleAWindowOfOnTheFly(const did_t firstDidOfWindow, const did_t nextWindowsFirst) {
	//decompress and calc scores
	unsigned* blockI; //find starting block of a window

	unsigned safeMaxDidPerBlock = (paddedLength/CONSTS::NUMERIC::BS);// != 1 ? (paddedLength/CONSTS::NUMERIC::BS)-1 : 1;
	if(firstDidOfWindow>maxDidPerBlock[safeMaxDidPerBlock]) //oldcode(paddedLength/CONSTS::NUMERIC::BS)-1] ) //can't make it
	{
		COUTV(5) << scratchPadId << " can't make it: " << firstDidOfWindow << ">" << maxDidPerBlock[safeMaxDidPerBlock] /*(paddedLength/CONSTS::NUMERIC::BS)-1]*/ << ENDL;
		postingsInWindow = 0;
		blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
		memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 1); //we need 2 times the space for block maxes -- two bytes each

		{ //this block is not scores related...
		//we are constructing on the fly -- that means no fake pbs
		postingBitSet = scratchPadPBS[scratchPadId];
		bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
		memset(postingBitSet,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		postingBitSet[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]=128; //always set the last one...

		while (nextWindowsFirst > maxDidPerBlock[compressedBlock+1]) ++compressedBlock;
		realDids = scratchPadDids[scratchPadId];
		realDids[0] = did = nextWindowsFirst;
		realScores = scratchPadScores[scratchPadId];
		realScores[0] = 0.0f;
		//offsetInPostingSpace = 0;
		} //this block is not scores related...
		return;
	}

	blockI = std::lower_bound(maxDidPerBlock,maxDidPerBlock+1+(paddedLength/CONSTS::NUMERIC::BS),firstDidOfWindow);
	unsigned block = blockI - maxDidPerBlock;

	if(block) --block; //are you sure?
	compressedBlock = block; //this is the first block of a did-s window in comp-postings-space
	assert(block<paddedLength/CONSTS::NUMERIC::BS);
	realDids = scratchPadDids[scratchPadId];
	realScores = scratchPadScores[scratchPadId];
	unsigned postings = 0;
	//special care for first block...
	entireBlockDidsAndScores<1>(didsBuffer,scoresBuffer,block++);
	unsigned i=0;
	while(didsBuffer[i]<firstDidOfWindow) ++i; //skip all those that are not in this window
	//COUTL << ENDL;
	assert(i<=CONSTS::NUMERIC::BS); //we mustn't skip more than a block -- if so, our block calc was flawed!
	//we will have written (CONSTS::NUMERIC::BS - i) postings and would like to be aligned to 16 bytes AFTER
	unsigned scoresGapInWin; //for compensation
	for(scoresGapInWin=0; ((CONSTS::NUMERIC::BS - i + scoresGapInWin)*sizeof(score_t))%16; ++scoresGapInWin) {}
	realScores += scoresGapInWin;

	for(; i<CONSTS::NUMERIC::BS; ++i) {
		realScores[postings]=scoresBuffer[i];
		realDids[postings++]=didsBuffer[i];
	}

	//and now all other blocks
	while(realDids[postings-1]<nextWindowsFirst) {
		entireBlockDidsAndScores<1>(realDids+postings,realScores+postings,block++);
		postings+=CONSTS::NUMERIC::BS;
	}
	//refine the last block:
	while(postings>0 && realDids[postings-1]>=nextWindowsFirst) --postings;
	postingsInWindow = postings; //getScores can use this threshold later

	blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
	memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 1); //we need 2 times the space for block maxes
	scoreInt_t dummy; //calculate block maxes

	for(size_t i=0; i<postingsInWindow; ++i) {
		const scoreInt_t scoreI = realScores[i];
		const did_t did = realDids[i];
		assert(did>=firstDidOfWindow && did<nextWindowsFirst);
		assert(((did-firstDidOfWindow)>>didBlockBits) < CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		scoreInt_t* buf[2] = {&dummy,blockMaxes+((did-firstDidOfWindow)>>didBlockBits)};
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}


	//we are constructing on the fly -- that means no fake pbs
	bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
	postingBitSet = scratchPadPBS[scratchPadId];
	memset(postingBitSet,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	buildPBS_M3(postingBitSet,realDids, firstDidOfWindow, didBlockBits, postingsInWindow);
	postingBitSet[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]|=128; //always set the last one...
	//if(scratchPadId==1)
	//for(unsigned i=0; i<postings; ++i)	COUT << realDids[i] << " " << realScores[i] << ENDL;
	//	COUTL << scratchPadId << "@" << currentWindow << " "  << realDids[0] << " " << realDids[postings-1] << ENDL;

	did = realDids[0];
	offsetInPostingSpace = 0;
	assert(compressedBlock<paddedLength/CONSTS::NUMERIC::BS);
	//COUTL << NewIteratorAugmented::debugGetBMForList(1,3067)<< ENDL;
}
//=============================================================================================
void NewIteratorAugmented::preprocessAugmentsForWindow() {	//build the structures for current window
	firstDidOfWindow = currentWindow*(CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits); //first did of current window
	const did_t nextWindowsFirst = std::min(CONSTS::NUMERIC::MAXD,firstDidOfWindow + (CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits));
	const unsigned blocksCnt = (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	//COUTD << currentWindow << " " << policy << ENDL;
	if(policy == ONTHEFLY)
		handleAWindowOfOnTheFly(firstDidOfWindow,nextWindowsFirst);
	else {
		if(firstDidOfWindow>maxDidPerBlock[(paddedLength/CONSTS::NUMERIC::BS)] ) {//can't make it
			COUT4 << scratchPadId << " non-otf-can't make it: " << firstDidOfWindow << ">" << maxDidPerBlock[(paddedLength/CONSTS::NUMERIC::BS)-1] << ENDL;
			blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
			memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 2); //we need 4 times the space for block maxes
			return;
		}
		//point the BM structure:
		if(policy ==  BM_FAKE || policy == BM_PBS || policy == BM_PBSC) {
			blockMaxes = blockScores[scratchPadId] = blockMaxesBase + (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);

		}
		else if(policy ==  BMQ_FAKE || policy == BMQ_PBS || policy == BMQ_PBSC) {//we want to dequantize this one:
			blockScores[scratchPadId] = blockMaxes = blockMaxesBase =  NewIteratorAugmented::scratchPadBMs[scratchPadId];

			dequantLinear(
					((unchar*)bmQorC) + blocksCnt,
					blockMaxes,
					CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK,quantileForBM);
		}
		else if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to decompress this first:
			blockScores[scratchPadId] = blockMaxes = blockMaxesBase = NewIteratorAugmented::scratchPadBMs[scratchPadId];
			unsigned *buffer = NewIteratorAugmented::scratchPadQBMs[scratchPadId];
			//const unsigned stopAtBlock = nextWindowsFirst < CONSTS::NUMERIC::MAXD ?  CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK : ((CONSTS::NUMERIC::MAXD-firstDidOfWindow)>>didBlockBits);
			const unsigned stopAtBlock = nextWindowsFirst >> didBlockBits;
			//COUTD << nextWindowsFirst << " stopAt " << stopAtBlock <<  ENDL;
/*
#ifdef DEBUG
	
                unsigned offDst = 0;
                unsigned skipsBuf[64];
                unsigned chunk = 0;
                unsigned nz = 0;
                unsigned maxBlock = currentWindow ? 0: stopAtBlock*10  ;
           while(offDst<maxBlock) {
               // COUTD << chunk << " " << offDst << ENDL;
                pack_decode_freqs(skipsBuf,res.compressedZeros+res.sizesForBMs[chunk],res.flagsForBMs[chunk]); //decompress to $
                ++chunk;
                for(unsigned j=0; j<CONSTS::NUMERIC::BS && offDst<maxBlock;j++) {
                        if(0 == skipsBuf[j]) {
                                if(res.quantBlockMaxes[offDst++] != res.nonZeroBlockMaxes[nz++]) FATAL("@");
                        }
                        else
                                offDst+=skipsBuf[j];
                        //COUTD << j << ": " << skipsBuf[j] << " " << offDst << ENDL;
                        }
                }
#endif
*/			assert(res.compressedZeros==bmQorC);
			assert(res.sizesForBMs==sizesCompBMs);
			assert(res.flagsForBMs==flagsBM);
			decompressQuantized(buffer,stateForBMQC,nonZeroesBase,bmQorC,flagsBM,sizesCompBMs,stopAtBlock);
			#ifdef DEBUG
			if(nextWindowsFirst<=CONSTS::NUMERIC::MAXD)
			for(unsigned t=0; t<4096; ++t)
				if(buffer[t]!=res.quantBlockMaxes[t+4096*currentWindow]) {
					COUTD << "b["<<t<<"]: " << buffer[t] << "!= q@" << t+4096*currentWindow << " "  << (int)res.quantBlockMaxes[t+4096*currentWindow] << ENDL; 
					//FATAL("decompress");
			/*
                unsigned offDst = 0;
                unsigned skipsBuf[64];
                unsigned chunk = 0;
                unsigned nz = 0;
                unsigned maxBlock = currentWindow ? 0: stopAtBlock*10  ;
           while(offDst<maxBlock) {
               // COUTD << chunk << " " << offDst << ENDL;
                pack_decode_freqs(skipsBuf,res.compressedZeros+res.sizesForBMs[chunk],res.flagsForBMs[chunk]); //decompress to $
                ++chunk;
                for(unsigned j=0; j<CONSTS::NUMERIC::BS && offDst<maxBlock;j++) {
                        if(0 == skipsBuf[j]) {
                                if(res.quantBlockMaxes[offDst++] != res.nonZeroBlockMaxes[nz++]) FATAL("@");
                        }
                        else
                                offDst+=skipsBuf[j];
                        //COUTD << j << ": " << skipsBuf[j] << " " << offDst << ENDL;
                        }
                }


					FATAL("$");
*/
				}
			#endif

			dequantLinear(buffer,blockMaxes,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK,quantileForBM);
		}

		//point the PBS structure
		if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE) {
			bitSetsPBS[scratchPadId] = (unchar*)fakePostingBitSet;
			postingBitSet = fakePostingBitSet;
		}
		else if(policy == BM_PBSC || policy == BMQ_PBSC || policy == BMQC_PBSC) {
			//decompress first todo
		}
		else {
			postingBitSet = (unsigned*)( (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) + ((unsigned char*)postingBitSetBase));
			bitSetsPBS[scratchPadId] = (unchar*) postingBitSet;
		}
	} //end if not on the fly
}
//=============================================================================================
void NewIteratorIntAugmented::preprocessAugmentsForWindow() {	//build the structures for current window
	firstDidOfWindow = currentWindow*(CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits); //first did of current window
	const did_t nextWindowsFirst = std::min(CONSTS::NUMERIC::MAXD,firstDidOfWindow + (CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits));
	const unsigned blocksCnt = (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	//COUTD << currentWindow << " " << policy << ENDL;
	if(policy == ONTHEFLY)
		handleAWindowOfOnTheFly(firstDidOfWindow,nextWindowsFirst);
	else {
		if(firstDidOfWindow>maxDidPerBlock[(paddedLength/CONSTS::NUMERIC::BS)] ) {//can't make it
			COUT4 << scratchPadId << " non-otf-can't make it: " << firstDidOfWindow << ">" << maxDidPerBlock[(paddedLength/CONSTS::NUMERIC::BS)-1] << ENDL;
			blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
			memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 1); //we need 2 times the space for block maxes
			return;
		}
		//point the BM structure:
		if(policy ==  BM_FAKE || policy == BM_PBS || policy == BM_PBSC) {
			blockMaxes = blockScores[scratchPadId] = blockMaxesBase + (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);

		}
		else if(policy ==  BMQ_FAKE || policy == BMQ_PBS || policy == BMQ_PBSC) {//we do NOT want to dequantize this one:
			//blockScores[scratchPadId] = blockMaxes = blockMaxesBase =  NewIteratorIntAugmented::scratchPadBMs[scratchPadId];
			blockMaxes = blockScores[scratchPadId] = blockMaxesBase + (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		}
		else if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to decompress this first:
			blockScores[scratchPadId] = blockMaxes = blockMaxesBase = NewIteratorIntAugmented::scratchPadBMs[scratchPadId];
			unsigned *buffer = NewIteratorIntAugmented::scratchPadQBMs[scratchPadId];
			 const unsigned stopAtBlock = nextWindowsFirst >> didBlockBits;
//			const unsigned stopAt = nextWindowsFirst < CONSTS::NUMERIC::MAXD ?  CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK : ((CONSTS::NUMERIC::MAXD-firstDidOfWindow)>>didBlockBits);
			//decompressQuantized(buffer,stateForBMQC,nonZeroesBase,bmQorC,flagsBM,sizesCompBMs,stopAt/64);
			decompressQuantized(buffer,stateForBMQC,nonZeroesBase,bmQorC,flagsBM,sizesCompBMs,stopAtBlock);
			//dequantLinear(buffer,blockMaxes,stopAt,quantileForBM); 
			for(unsigned i=0; i<CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK; ++i)
				blockMaxes[i] = buffer[i]; //can be sped up...
		}

		//point the PBS structure
		if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE) {
			bitSetsPBS[scratchPadId] = (unchar*)fakePostingBitSet;
			postingBitSet = fakePostingBitSet;
		}
		else if(policy == BM_PBSC || policy == BMQ_PBSC || policy == BMQC_PBSC) {
			//decompress first todo
		}
		else {
			postingBitSet = (unsigned*)( (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) + ((unsigned char*)postingBitSetBase));
			bitSetsPBS[scratchPadId] = (unchar*) postingBitSet;
		}
	} //end if not on the fly
}
//=============================================================================================
inline unchar cvtEpi8ToEpi16Mask(int smask) {
        unchar mask = (smask>>13)&3;
        mask <<= 2;
        mask |= (smask>>9)&3;
        mask <<= 2;
        mask |= (smask>>5)&3;
        mask <<= 2;
        mask |= (smask>>1)&3;
        return mask;
}
//=============================================================================================
inline __m128i duplicateShortAndMaskWithChar(unsigned short val, unsigned char mask) {
	unsigned short dups[8] = { val, val, val, val, val, val, val, val };
	for(unsigned i=0; i<8; ++i, mask>>=1) 
		dups[i] *= mask%2;
	return _mm_load_si128((__m128i*)dups);
}
//=============================================================================================
const __m128 floatMaskingWithNibbleMasks[16] = {
		{ 0.0, 0.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0, 0.0 }, 	{ 0.0, 1.0, 0.0, 0.0 }, { 1.0, 1.0, 0.0, 0.0 },
		{ 0.0, 0.0, 1.0, 0.0 }, { 1.0, 0.0, 1.0, 0.0 }, 	{ 0.0, 1.0, 1.0, 0.0 }, { 1.0, 1.0, 1.0, 0.0 },

		{ 0.0, 0.0, 0.0, 1.0 }, { 1.0, 0.0, 0.0, 1.0 }, 	{ 0.0, 1.0, 0.0, 1.0 }, { 1.0, 1.0, 0.0, 1.0 },
		{ 0.0, 0.0, 1.0, 1.0 }, { 1.0, 0.0, 1.0, 1.0 }, 	{ 0.0, 1.0, 1.0, 1.0 }, { 1.0, 1.0, 1.0, 1.0 },
};

inline __m128 duplicateFloatAndMaskWithNibble(float val, unsigned char nibble) {
	//set_ps version is a little bit slower perhaps
	//const float vals[2] = {0, val}; return _mm_set_ps(vals[(nibble>>3)&1], vals[(nibble>>2)&1], vals[(nibble>>1)&1], vals[nibble&1]);
	const __m128 dups = { val, val, val, val };
	return _mm_mul_ps(dups, floatMaskingWithNibbleMasks[nibble]);
}
//=============================================================================================
void NewIteratorIntAugmented::buildLA_SSE(const scoreInt_t threshold, const unsigned termsCount) {
	//Make sure that when this setup is used the right translation to did space is done for the bit in getNextLive !!!
	const unsigned windowsize = CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	assert(windowsize%8 == 0);
	unchar* ptr = reinterpret_cast<unchar*>(liveDeadArea);

	__m128i thresholds = _mm_set_epi16(threshold,threshold,threshold,threshold, threshold,threshold,threshold,threshold);
	__m128i a = _mm_set_epi16(0,0,0,0,0,0,0,0);

#if MTHREE==0
	for(unsigned i=0; i<windowsize; i+=8) {
		//scalar version: 
	/*	
		unchar flags = 0;
		for(int b=7; b>=0; --b) {
			scoreInt_t sum = 0;
			for(unsigned char lst=0; lst<termsCount; ++lst) 
                        	sum += *(blockScores[lst]+i+b);
			assert(sum<2560);
			flags=(flags<<1) | (sum>threshold);
		}
		ptr[i>>3]=flags;
	*/	

		a = _mm_xor_si128(a,a);
		for(unsigned char lst=0; lst<termsCount; ++lst) 
			a = _mm_add_epi16(a, _mm_load_si128((__m128i*) (blockScores[lst]+i))); //read 8 shorts, add 8 shorts
		
		__m128i masks = _mm_cmpgt_epi16(a,thresholds); //cmp to thresholds
		int maskBits = _mm_movemask_epi8( masks ); //get 16 masks
		ptr[i>>3] = cvtEpi8ToEpi16Mask(maskBits); //todo - check/optimize convert epi8 to 16!

	} //note: need to cast to uchar
	ptr[(windowsize-1)>>3]|=128;
#else
	//Make sure that when this setup is used the right translation to did space is done for the bit in getNextLive !!!
	
	//NOTE: while can't find the ge version of cmp, use smaller thresholds (-1s, I guess)
	for(unsigned i=0; i<windowsize; i+=8) {
		//---------------------- level one filter
		a = _mm_xor_si128(a,a);
		for(unsigned lst = 0; lst<termsCount; ++lst) 
			a = _mm_add_epi16(a, _mm_load_si128((__m128i*) (blockScores[lst]+i)));
	
		int maskBitsL1 = _mm_movemask_epi8( _mm_cmpgt_epi16(a,thresholds) ); //have the mask for survivors in this area
	
		//if none the loop doesn't run
		for(unsigned bit=0; bit<8 && maskBitsL1>0; ++bit) {
			bool flag = maskBitsL1%2;
			maskBitsL1 >>=2; //the annoying epi8...
			if(flag==0)	continue;

			const unsigned didC = i+bit;
			a = _mm_xor_si128(a,a);
			for(unsigned lst = 0; lst<termsCount; ++lst) {
				const unchar pBits = bitSetsPBS[lst][didC];
				a = _mm_add_epi16(a, duplicateShortAndMaskWithChar((blockScores[lst])[didC],pBits));
			}
			__m128i masks = _mm_cmpgt_epi16(a,thresholds); //cmp to thresholds
			int maskBits = _mm_movemask_epi8( masks ); //get 16 masks
			ptr[didC] = cvtEpi8ToEpi16Mask(maskBits); //todo - check/optimize convert epi8 to 16!
		}
	}
	ptr[windowsize-1]|=128;
#endif
//	COUTL << currentWindow <<" "<<threshold<<" "<<countSetBits((void*)ptr,windowsize>>(3-MTHREE)) << ENDL;
}
//=============================================================================================
void NewIteratorAugmented::buildLA_SSE(const float threshold, const unsigned termsCount) {
#if MTHREE==0 // only LA without posting bitset
	//Make sure that when this setup is used the right translation to did space is done for the bit in getNextLive !!!
	const unsigned windowsize = CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	assert(windowsize%8 == 0);
	unchar* ptr = reinterpret_cast<unchar*>(liveDeadArea);

	__m128 thresholds = {threshold,threshold,threshold,threshold};
	__m128 a = (__m128){ .0f,.0f,.0f,.0f }; //a = _mm_xor_ps(a,a);
	__m128 b;
	for(unsigned i=0; i<windowsize; i+=8) {
		a = _mm_xor_ps(a,a);
		for(unsigned char lst=0; lst<termsCount; ++lst) {
			b = _mm_load_ps(blockScores[lst]+i);
			a = _mm_add_ps(a, b);
		}
		__m128 masks = _mm_cmpge_ps(a,thresholds);
		int maskBitsLow = _mm_movemask_ps( masks );
		a = _mm_xor_ps(a,a);
		for(unsigned char lst=0; lst<termsCount; ++lst) {
			b = _mm_load_ps(blockScores[lst]+i+4);
			a = _mm_add_ps(a, b);
		}
		masks = _mm_cmpge_ps(a,thresholds);
		int maskBits = (_mm_movemask_ps( masks )<<4);
		ptr[i>>3] = maskBits+maskBitsLow;
	} //note: need to cast to uchar
	ptr[(windowsize-1)>>3]|=128;
	//COUTL << currentWindow << " " <<threshold << " " << countSetBits((void*)ptr,windowsize>>3) << ENDL;
#else // LA + posting bitset
	//Make sure that when this setup is used the right translation to did space is done for the bit in getNextLive !!!

	//NOTE: compge can sometimes fail and for super safety should be replaced with fabs(a-threshold)<epsilon
	//the workaround is not to use perfectly clairvoyant threshold -- always have it -epsiloned
	unchar* ptr = reinterpret_cast<unchar*>(liveDeadArea);

	const unsigned windowsize = CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	__m128 a = (__m128){ .0f,.0f,.0f,.0f };
	__m128 thresholds = {threshold,threshold,threshold,threshold};
	for(unsigned i=0; i<windowsize; i+=4) {
		//---------------------- level one filter
		a = _mm_xor_ps(a,a);
		for(unsigned lst = 0; lst<termsCount; ++lst) {
			a = _mm_add_ps(a, _mm_load_ps(blockScores[lst]+i));
		}
		__m128 masksL1 = _mm_cmpge_ps(a,thresholds);
		int maskBitsL1 = _mm_movemask_ps( masksL1 ); //have the mask for survivors in this area

//			for(unsigned lst = 0; lst<termsCount; ++lst) {
//				if(blockScores[lst][i] > 77.0) {	COUTL << i <<"@" << lst << " " << blockScores[lst] << ENDL;
//					FATAL("too much " + toString(blockScores[lst][i])) ; }
//			}


		//if none the loop doesn't run
		for(unsigned bit=0; bit<4 && maskBitsL1>0; ++bit) {
			bool flag = maskBitsL1%2;
			maskBitsL1 >>=1;
			if(flag==0)	continue;

			const unsigned didC = i+bit;
			//---------------------- lower four -------------------
			a = _mm_xor_ps(a,a);
			for(unsigned lst = 0; lst<termsCount; ++lst) {
				const unsigned char pBits = bitSetsPBS[lst][didC];
				//four lower
				const unsigned char lower = pBits & 15;
				a = _mm_add_ps(a, duplicateFloatAndMaskWithNibble((blockScores[lst])[didC],lower));
			}
			__m128 masksL = _mm_cmpge_ps(a,thresholds);
			int maskBitsLow = _mm_movemask_ps( masksL );
			//---------------------- upper four -------------------
			a = _mm_xor_ps(a,a);
			for(unsigned lst = 0; lst<termsCount; ++lst) {
				const unsigned char pBits = bitSetsPBS[lst][didC];
				//four upper
				const unsigned char upper = pBits>>4;
				a = _mm_add_ps(a, duplicateFloatAndMaskWithNibble((blockScores[lst])[didC],upper));
			}
			__m128 masksU = _mm_cmpge_ps(a,thresholds);
			unsigned char maskBitsHi = (_mm_movemask_ps( masksU )<<4);
			//------------------------------------------------------
			ptr[didC] = maskBitsHi+maskBitsLow;
		}
	}
	ptr[windowsize-1]|=128;
	//COUTL << currentWindow <<" "<<threshold<<" "<<countSetBits((void*)ptr,windowsize) << ENDL;
#endif
}
//=============================================================================================
void NewIteratorIntAugmented::initScratchpads() {
	//assuming the bits were set already...
	for(unsigned i=0; i<16; ++i) {
		NewIteratorIntAugmented::scoresBuffer = NEWM scoreInt_t[CONSTS::NUMERIC::BS];
		NewIteratorIntAugmented::scratchPadBMs[i] = NEWM scoreInt_t[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK*sizeof(scoreInt_t)];
		NewIteratorIntAugmented::scratchPadScores[i] = NEWM scoreInt_t[(1+CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK)<<didBlockBits];
	}
}
//=============================================================================================
void NewIteratorAugmented::initScratchpads() {
	//assuming the bits were set already...
	for(unsigned i=0; i<16; ++i) {
		NewIteratorAugmented::scoresBuffer = NEWM score_t[CONSTS::NUMERIC::BS];
		NewIteratorAugmented::scratchPadPBS[i] = NEWM unsigned[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK/sizeof(unsigned)];
		NewIteratorAugmented::scratchPadBMs[i] = NEWM score_t[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK*sizeof(score_t)];
		NewIteratorAugmented::scratchPadDids[i] = NEWM did_t[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK<<didBlockBits];
		NewIteratorAugmented::scratchPadScores[i] = NEWM score_t[(1+CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK)<<didBlockBits];
		NewIteratorAugmented::scratchPadQBMs[i] = NEWM unsigned[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK];
		NewIteratorAugmented::liveDeadArea = NEWM unsigned[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK]; //fits both version with/without postings
	}

	unsigned char* fakePostingBitSetp = NEWM unsigned char[CONSTS::NUMERIC::MAXD>>3]; //more than enough
	memset(fakePostingBitSetp,255,CONSTS::NUMERIC::MAXD>>3);
	fakePostingBitSet = reinterpret_cast<unsigned*>(fakePostingBitSetp);
}
//=============================================================================================
void dequantLinear(const unsigned char* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile) {
	__m128 quant4 = {quantile,quantile,quantile,quantile};
	__m128i zero = _mm_set1_epi8(0);
	const unsigned size =(sizeUnpadded>>4)<<4; //make 16 aligned
	unsigned q;
	for(q=0; q<size; q+=16) {
		//first make 16 uint_16 out of 16 uint_8
		__m128i x = _mm_load_si128( (__m128i*)(arrSrc+q));
		__m128i xlo = _mm_unpacklo_epi8(x, zero);
		__m128i xhi = _mm_unpackhi_epi8(x, zero);

		//then make 16 uint_32 out of 16 uint_16
		__m128i xllo = _mm_unpacklo_epi16(xlo, zero);
		__m128i xlhi = _mm_unpackhi_epi16(xlo, zero);
		__m128i xhlo = _mm_unpacklo_epi16(xhi, zero);
		__m128i xhhi = _mm_unpackhi_epi16(xhi, zero);

		//convert to float
		__m128 yllo = _mm_cvtepi32_ps(xllo);
		__m128 ylhi = _mm_cvtepi32_ps(xlhi);
		__m128 yhlo = _mm_cvtepi32_ps(xhlo);
		__m128 yhhi = _mm_cvtepi32_ps(xhhi);

		//quantize and store
		float* ptr = arrDst+q;
		_mm_store_ps(ptr,   (_mm_mul_ps(yllo,quant4)));
		_mm_store_ps(ptr+4, (_mm_mul_ps(ylhi,quant4)));
		_mm_store_ps(ptr+8, (_mm_mul_ps(yhlo,quant4)));
		_mm_store_ps(ptr+12,(_mm_mul_ps(yhhi,quant4)));
	}
	//and if there are "spill overs" do them here
	for(; q<sizeUnpadded; ++q) arrDst[q]=arrSrc[q]*quantile;
}
//=============================================================================================
void dequantLinear(const unsigned* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile) {
	__m128 quant4 = {quantile,quantile,quantile,quantile};
	const unsigned size = (sizeUnpadded>>2)<<2; //make 4 aligned
	unsigned q;
	for(q=0; q<size; q+=4) {
		__m128i x = _mm_load_si128( (__m128i*)(arrSrc+q)); //load 4 unsigned
		__m128 y = _mm_cvtepi32_ps(x); //convert to float
		float* ptr = arrDst+q;
		_mm_store_ps(ptr,   (_mm_mul_ps(y,quant4)));//quantize and store
	}
	//and if there are "spill overs" do them here
	for(; q<sizeUnpadded; ++q) arrDst[q]=arrSrc[q]*quantile;
}
//=============================================================================================
PreprocessingData NewIteratorAugmented::buildInitialAugnemtsData() {
	PreprocessingData res;
	#ifndef USEUNCOMPRESSEDQUANTS
		if(policy == NONE || policy == ONTHEFLY) return res; //nothing to do for those
	#endif
	//we will need dids and freqs for all policies
	did_t* dids = NEWM did_t[paddedLength];
	score_t* scores = NEWM score_t[paddedLength];
	for(unsigned block=0; block<paddedLength/CONSTS::NUMERIC::BS; ++block) {
		entireBlockDidsAndScores(dids + (CONSTS::NUMERIC::BS*block), scores + (CONSTS::NUMERIC::BS*block), block);
	}
        #ifdef USEUNCOMPRESSEDQUANTS
	//testing Torsten's request
	res.rawQuants = NEWM unchar[paddedLength];
        for(unsigned i=0; i<paddedLength; ++i)
              res.rawQuants[i]=scores[i];
	#endif
	if(policy == NONE || policy == ONTHEFLY) return res; //nothing to do for those
	//how many blocks we have when the number is padded to num of blocks in cache friendly window
	const unsigned SIZE = (((CONSTS::NUMERIC::MAXD>>didBlockBits)>>CONSTS::NUMERIC::CACHE_FRIENDLY_BITS)+1)<<CONSTS::NUMERIC::CACHE_FRIENDLY_BITS;
	blockMaxesBase = NEWM score_t[SIZE];
	memset(blockMaxesBase,0,SIZE*sizeof(score_t));
	blockMaxes = blockMaxesBase;
	genMaxesWithPtr(dids, scores, paddedLength);

	res.blockMaxes = blockMaxesBase;
	DEL scores; //done using it
	if(policy !=  BM_FAKE && policy != BM_PBS && policy != BM_PBSC) //we want to quantize this one:
	{
		quantizeMaxes(blockMaxes,SIZE,res,maxScoreOfList);


		if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to compress this one:
			compressQuantized(res.quantBlockMaxes,SIZE,res);
#ifdef DEBUG
		unsigned offDst = 0;
		unsigned skipsBuf[64];
		unsigned chunk = 0;
		unsigned nz = 0;
		unsigned maxBlock = 4096*8;
           while(offDst<maxBlock) {
                //COUTD << chunk << ENDL;
                pack_decode_freqs(skipsBuf,res.compressedZeros+res.sizesForBMs[chunk],res.flagsForBMs[chunk]); //decompress to $
		++chunk;
                for(unsigned j=0; j<CONSTS::NUMERIC::BS && offDst<maxBlock;j++) {
                        if(0 == skipsBuf[j]) {
                                if(res.quantBlockMaxes[offDst++] != res.nonZeroBlockMaxes[nz++]) FATAL("@");
                        }
                        else
                                offDst+=skipsBuf[j];
                        //COUTD << j << ": " << skipsBuf[j] << " " << offDst << ENDL;
                        }
                }
        

			unsigned *ptr = NEWM unsigned[SIZE];
			unsigned tmp[4] = {0,0,0,0};
			ktuple<unsigned,4> offsets(tmp);
/*
			for(unsigned win=0; win<10; ++win) {
				const unsigned block = 4096;
				unsigned wrote = decompressQuantized(ptr,offsets,
					res.nonZeroBlockMaxes,res.compressedZeros,res.flagsForBMs,
					res.sizesForBMs,block*(1+win));
				COUT << "wrote: " << wrote << " comp. block: " << offsets[1] << " off in block: " << offsets[2] << " skip over: " << offsets[3]<< ENDL;   
				for(unsigned d=0; d<block; ++d)
					if(res.quantBlockMaxes[d+(block*win)] != ptr[d]) {
						COUTL << win*block+d << " " << (int)res.quantBlockMaxes[d+(block*win)] <<" != ptr:" <<ptr[d] << ENDL;
						FATAL("");
					}
				
			}
			*/
			DEL ptr;
#endif
#ifndef DEBUG
			DEL res.quantBlockMaxes;  //don't need it in this case
#endif
			DEL res.blockMaxes; //don't need it in this case
		}
	}

	postingBitSetBase = NEWM unsigned[SIZE];
	memset(postingBitSetBase,0,SIZE*sizeof(unsigned));
	postingBitSet = postingBitSetBase;
	if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE) {
		res.postingBitSet = fakePostingBitSet; //make sure nobody deletes this one ;)
	}
	else {
		buildPBS_M3(postingBitSetBase, dids, 0, didBlockBits, paddedLength);
		res.postingBitSet = postingBitSetBase;

		if(policy == BM_PBSC || policy == BMQ_PBSC || policy == BMQC_PBSC) {

			DEL res.postingBitSet; //don't need it now
		}
	}

	resetPostingIterator();

//#ifdef DEBUG
	////sanity:
	//did_t did=-1;
	//for(unsigned i=0; i<paddedLength; ++i) {
		//if(dids[i]!= (did = nextGEQ(did+1)) ) {
			//COUTL << i << " "  << dids[i] << " "  << did << ENDL;
			//throw; break;
		//}
	//}
	//resetPostingIterator();
//#endif

	DEL dids;

	return res;
}

//=============================================================================================
PreprocessingData NewIteratorIntAugmented::buildInitialAugnemtsData() {
	PreprocessingData res;
        #ifndef USEUNCOMPRESSEDQUANTS
	if(policy == NONE || policy == ONTHEFLY) return res; //nothing to do for those
	#endif
	//we will need dids and freqs for all policies
	did_t* dids = NEWM did_t[paddedLength];
	scoreInt_t* scores = NEWM scoreInt_t[paddedLength];
	for(unsigned block=0; block<paddedLength/CONSTS::NUMERIC::BS; ++block) {
		entireBlockDidsAndScores<0>(dids + (CONSTS::NUMERIC::BS*block), scores + (CONSTS::NUMERIC::BS*block), block);
	}
        #ifdef USEUNCOMPRESSEDQUANTS
        //testing Torsten's request
        res.rawQuants = NEWM unchar[paddedLength];
        for(unsigned i=0; i<paddedLength; ++i)
              res.rawQuants[i]=scores[i];
        #endif
	 if(policy == NONE || policy == ONTHEFLY) return res; //nothing to do for thosey
	//how many blocks we have when the number is padded to num of blocks in cache friendly window
	const unsigned SIZE = (((CONSTS::NUMERIC::MAXD>>didBlockBits)>>CONSTS::NUMERIC::CACHE_FRIENDLY_BITS)+1)<<CONSTS::NUMERIC::CACHE_FRIENDLY_BITS;
	blockMaxesBase = NEWM scoreInt_t[SIZE];
	memset(blockMaxesBase,0,SIZE*sizeof(scoreInt_t));
	blockMaxes = blockMaxesBase;
	genMaxesWithPtr(dids, scores, paddedLength);
	//#ifdef DEBUG
		for(unsigned t=0; t<SIZE; ++t)
			if(blockMaxes[t]>256){ COUTL << t << " " << blockMaxes[t] << ENDL; FATAL("wrong blockmax"); }
	//#endif
	res.globalQuantBlMaxes = blockMaxesBase;
	DEL scores; //done using it
	if(policy !=  BM_FAKE && policy != BM_PBS && policy != BM_PBSC) //we want to quantize this one:
	{
		//they are already quantized
		if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to compress this one:
			compressQuantized(res.globalQuantBlMaxes,SIZE,res);
		}
	}

	//this part is doubled from float -- refactor
	postingBitSetBase = NEWM unsigned[SIZE];
	memset(postingBitSetBase,0,SIZE*sizeof(unsigned));
	postingBitSet = postingBitSetBase;
	if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE) {
		res.postingBitSet = fakePostingBitSet; //make sure nobody deletes this one ;)
	}
	else {
		buildPBS_M3(postingBitSetBase, dids, 0, didBlockBits, paddedLength);
		res.postingBitSet = postingBitSetBase;

		if(policy == BM_PBSC || policy == BMQ_PBSC || policy == BMQC_PBSC) {

			DEL res.postingBitSet; //don't need it now
		}
	}
	resetPostingIterator();
	DEL dids;
	return res;
}
//=============================================================================================
template <typename T>
inline void NewIteratorAugmentedBase::typedGenMaxesWithPtr(const did_t* docIds, const T* scores, T* blockMaxes,  const unsigned size, const did_t smallest, const T assrtBound) {
	assert(size%4==0);
	//assume blockMaxes are initialized
	T dummy;
	for(size_t i=0; i<size; ++i) {
		const T scoreI = scores[i];
		assert(scoreI<assrtBound);
		const did_t did = docIds[i]-smallest;
		T* buf[2] = {&dummy,blockMaxes+(did>>didBlockBits)}; 
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}	
}
//=============================================================================================
void NewIteratorIntAugmented::genMaxesWithPtr(const did_t* docIds, const scoreInt_t* scores, const unsigned size, const did_t smallest) {
	typedGenMaxesWithPtr<scoreInt_t>(docIds, scores, blockMaxes, size, smallest,256);
}
//=============================================================================================
void NewIteratorAugmented::genMaxesWithPtr(const did_t* docIds, const score_t* scores, const unsigned size, const did_t smallest) {
	typedGenMaxesWithPtr(docIds, scores, blockMaxes, size, smallest, 80.0f);
/*
	assert(size%4==0);
	//assume blockMaxes are initialized
	score_t dummy;
	for(size_t i=0; i<size; ++i) { //&& docIds[i]<CONSTS::NUMERIC::MAXD
		const score_t scoreI = scores[i];
		assert(scoreI<70.0);
		const did_t did = docIds[i]-smallest;
		score_t* buf[2] = {&dummy,blockMaxes+(did>>didBlockBits)}; //note that this will not work in windowed mode -- we must sub. the smalles did of window
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}
	*/
}
#endif

//=============================================================================================
//float blockMaxScoresLinearQuantizer::getSpaceImprovement() const {
//	return 100.0*((float)compressedSize) / ((float) size);
//}
//=============================================================================================
void NewIteratorAugmented::quantizeMaxes(const float* scores, const unsigned sz,  PreprocessingData& prData, const float maxScoreOfList){
	float large = (maxScoreOfList>0) ? maxScoreOfList : (*std::max_element(scores,scores+sz));
	score_t quantile = ((score_t) large)/((score_t)CONSTS::NUMERIC::QUANTIZATION);
	const unsigned size = sz;
	const unsigned padSize = size%CONSTS::NUMERIC::BS ? ((size>>6)+1)<<6 : size;

	prData.quantile =quantile;
	unsigned char* ptr = NEWM unsigned char[padSize];
	prData.quantBlockMaxes = ptr;
	unsigned i=0;
	for(unsigned j=0; j<size; ++j) {
		float temp = (scores[j]/quantile);
		unsigned int quantized_value = (unsigned int) temp;
		float remainder = temp - quantized_value;
		ptr[i++] = ( (Fcompare(remainder, 0.0f) != 0)&&(quantized_value!=CONSTS::NUMERIC::QUANTIZATION) ) ?
				(unsigned char) quantized_value + 1 : (unsigned char) quantized_value;
	}
	//for(unsigned i=0; i<size; ++i) dest[i]=quantVals[i]*quantile; -- scalar dequant
}
//=============================================================================================
template <typename Q>
unsigned NewIteratorAugmentedBase::compressQuantized(const Q* quantized, const unsigned sz, PreprocessingData& prData) {
	//assuming sz is the padded and aligned version
	std::vector<unchar> nonZeroes;
	vecUInt skips;
	nonZeroes.reserve(sz);
	skips.reserve(sz);
	unsigned skipsCount=0;
	for(unsigned s=0; s<sz; ++s) {
		if(quantized[s]) {
			if(skipsCount)
				skips.push_back(skipsCount);
			skips.push_back(0);
			skipsCount = 0;
			nonZeroes.push_back(quantized[s]); //maybe conversion from short to unchar
		}
		else
			++skipsCount;
	}
	//pad
	while(skips.size()%CONSTS::NUMERIC::BS) { skips.push_back(1); }
	skips.push_back(1);	
	//pad more
	while(skips.size()%CONSTS::NUMERIC::BS) { skips.push_back(1); }
	
	#ifdef DEBUG
	unsigned offDst =0;
	unsigned nzOff = 0;
	std::vector<unchar> dstR(sz); 
	for(unsigned j=0; j<sz && offDst<sz; ++j)
                if(0 == skips[j]) {
			dstR[offDst++] = nonZeroes[nzOff++];
		}
                else
                        offDst+=skips[j];
	for(unsigned j=0; j<(CONSTS::NUMERIC::MAXD>>6); ++j)
		if(quantized[j]!=dstR[j]) { 
			COUTD << j << ": " << (int)quantized[j]<<"!="<<(int)dstR[j]<<ENDL;
			//if(j < 300123) FATAL("!"); 
		}
	COUTD << "Tested: " << sz << ENDL;
	#endif

	//now we have decomposed all the nonzeroes and the skips. we need to blockwise compress the skips:
	unsigned* compressedData = NEWM unsigned[sz]; //tmp destination vector
	unsigned  *dstPtr = compressedData;

	vecUInt flags;
	vecUInt offsets;

	flags.reserve(sz/CONSTS::NUMERIC::BS);
	offsets.reserve(sz/CONSTS::NUMERIC::BS);
	offsets.push_back(0);
	const unsigned skipsSz = (skips.size()/CONSTS::NUMERIC::BS +1)*CONSTS::NUMERIC::BS;
	for(unsigned i=0; i<skipsSz; i+=CONSTS::NUMERIC::BS) {
		int flag = -1; //this is stupid ! -- we should use the trick of countBitsToPack
		for(int j=0; flag<0; ++j)
			flag = pack_encode(&dstPtr,&skips[i],j);
		flags.push_back(flag);
		assert((dstPtr - compressedData)-offsets.back() <= 64);
		offsets.push_back(dstPtr-compressedData);
	}

	prData.compressedZeros = compressedData;
	prData.flagsForBMs = NEWM unsigned[flags.size()];
	std::copy(&flags[0],&flags[0]+flags.size(),prData.flagsForBMs); //better to create new vector and swap to it
	prData.sizesForBMs = NEWM unsigned[offsets.size()];
	std::copy(&offsets[0],&offsets[0]+offsets.size(),prData.sizesForBMs); //better to create new vector and swap to it
	prData.nonZeroBlockMaxes = NEWM unchar[nonZeroes.size()];
	std::copy(&nonZeroes[0],&nonZeroes[0]+nonZeroes.size(),prData.nonZeroBlockMaxes); //better to create new vector and swap to it

#ifdef DEBUG
	for(unsigned i=1; i<offsets.size(); ++i)
		if(prData.sizesForBMs[i]<=prData.sizesForBMs[i-1]) FATAL("?");
	std::vector<unsigned> skipR;
	unsigned skipsBuf[CONSTS::NUMERIC::BS];
	for(unsigned chunk=0; chunk < flags.size(); ++chunk) {
		pack_decode_freqs(skipsBuf,compressedData+offsets[chunk],flags[chunk]);
		for(unsigned k=0; k<CONSTS::NUMERIC::BS; ++k)
			skipR.push_back(skipsBuf[k]);
	}
        for(unsigned j=0; j<skips.size(); ++j)
                if(skips[j]!=skipR[j]) FATAL("!");
        COUTD << "Tested2: " << sz << ENDL;

	
#endif

	//sizes in bytes!
	const unsigned skipsCompressedSize = offsets.back();
	const unsigned nonZeroesSize = nonZeroes.size();
	const unsigned bitsSize = flags.size();
	unsigned compressedSize = skipsCompressedSize + bitsSize + nonZeroesSize;
	//COUTD << "flags.size() " << flags.size() << " skips size: " << skips.size() << " nonZeroesSize "  <<  nonZeroesSize << ENDL;
	//COUTD << compressedSize << " " << nonZeroesSize << "+" << skipsCompressedSize << " ->" << skipsSz << ENDL;
	return compressedSize;
}
//=============================================================================================
void NewIteratorAugmentedBase::decompressQuantized(unsigned* dst, ktuple<unsigned,4>& offsets,
									const unchar* nonZeroBlockMaxes, unsigned* compressedZerosBase,
									const unsigned* flagsForBMs, unsigned* sizesForBMs, const unsigned lastBlockMax) {
	//note: that as the offsets (size) are absolute, here we must have the base ptr to compressedZeros, while the sizesForBMs is a relative to a window
	//offsets[0] => offset in nonZeroes where we stopped
	//offsets[1] => current compressed block for skips
	//offsets[2] => offset in last decompressed
	//offsets[3] => offset in dst buffer
	static unsigned skipsBuf[CONSTS::NUMERIC::BS];
	unsigned offDst = offsets[3];
	unsigned j=0;
	memset(dst,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK*sizeof(unsigned));
	//special case: we skipped an entire window (or more)
	//we must skip to this place, but without writing to mem
	if(offDst>lastBlockMax) { //then the window (droids) you are looking for is all 0 anyway
		COUTD << "lastblock: "  << lastBlockMax << " offDst: " << offDst <<" <=" << (int)lastBlockMax-(int)offDst << ENDL; 
                return;

	}
	if(lastBlockMax-offDst>CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK){  //we skipped window!
		COUTL << lastBlockMax << "-" << offDst <<"=" << lastBlockMax-offDst << ENDL; 
		FATAL("^");
	}

	//now continue with the next blocks...
	while(offDst<lastBlockMax) {
		j = offsets[2]; //get the offset from either prev. iteration or last call
		pack_decode_freqs(skipsBuf,compressedZerosBase+sizesForBMs[offsets[1]],flagsForBMs[offsets[1]]); //decompress to tmp buffer
		offsets[1]+=1; //inc compressed block
		for(;j<CONSTS::NUMERIC::BS && offDst<lastBlockMax;j++) {
			if(0 == skipsBuf[j]) { 
				dst[offDst%CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK] = nonZeroBlockMaxes[offsets[0]++]; //note: as dequant expects to get unsigned vals we inflate the byte to 4
				++offDst;
			}
			else
				offDst+=skipsBuf[j];
			//COUTD << j << ": " << skipsBuf[j] << " " << offDst << ENDL;
		}
		offsets[2]=0; //reset j for next iteration
	}

	//COUTD << "j: " << j << " skipsBuf[j]: " << skipsBuf[j] << " skipsBuf[j+1]: " << skipsBuf[j+1] << " nz: " << (int)nonZeroBlockMaxes[offsets[0]] << ENDL;
	//special case: prev. window was a skip over window boundary
	offsets[3] = offDst;//-lastBlockMax;
	offsets[1]-=1;//get back one block
	offsets[2]=j;//register leftovers of it
}
//=============================================================================================
unsigned NewIteratorAugmentedBase::compressPBS(const unsigned* pbs, const unsigned sz, PreprocessingData& prData) {
	FATAL("under construction");
}
