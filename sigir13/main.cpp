#include <fstream>

#include "options.h"
#include "Executor.h"

std::ofstream logFile("logfile.txt", std::ios_base::out |std::ios_base::app);

int main(int argc, char * argv[] ){
	argc-=(argc>0); argv+=(argc>0); // skip program name argv[0]
	Options options1;
	options1.parseFromCmdLine(argc,argv);
	globalVerbosity = options1.VERBOSITY;
	CONSTS::INDEX::setTrecRoot(options1.INDEXDIR);
	std::string queryFile = options1.QLOG;
	LOG << getTimeString() << ENDL;
	NewIteratorAugmented::setBits(options1.DIDBLOCKBITS);
	if(options1.IMBUE)
		std::cout.imbue(std::locale(""));
	Executor qp1(options1,queryFile);
	#ifdef USEINTPIPE
		COUTL << "USEINTPIPE" << ENDL;
		qp1.runInt();
	#else
		COUTL << "FLOATP" << ENDL;
		qp1.run();
	#endif
	qp1.printReport();
	return 0;
}

