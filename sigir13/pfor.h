#ifndef _PFOR_H_
#define _PFOR_H_

/*
Its gonna take time
A whole lot of precious time
Its gonna take patience and time, ummm
To do it, to do it, to do it, to do it, to do it,
To do it right child!
George Harrison
*/

int pack_encode(unsigned int **w, unsigned int *p, int num);
void pack(const unsigned*src, const unsigned bits, const unsigned size, unsigned int *dest);
void unpackHubB(unsigned *dstDecomressedPtr, unsigned *srcCompressedPtr, const unsigned char bits);

template <typename T>
unsigned char countBitsToPack(const T* chunk, const unsigned size) {
	//assert(sizeof(T)<5);
	unsigned char maxbits = 0;
	for(unsigned i=0; i<size; ++i)
		if(chunk[i] && 32-__builtin_clz(chunk[i]) > maxbits)
			maxbits = 32-__builtin_clz(chunk[i]);
	return maxbits;
}

unsigned int *pack_decode_dids_or_freqs(unsigned int *dstDecopmressedPtr, unsigned int *srcCompressedPtr, int flag, int base);

inline unsigned int *pack_decode_freqs(unsigned int *destDecomressedPtr, unsigned int *srcCompressedPtr, int flag) {
	return pack_decode_dids_or_freqs(destDecomressedPtr,srcCompressedPtr,flag,0);
}

inline unsigned int *pack_decode_dids(unsigned int *destDecomressedPtr, unsigned int *srcCompressedPtr, int flag,int base) {
	return pack_decode_dids_or_freqs(destDecomressedPtr,srcCompressedPtr,flag,base);
}

#endif
