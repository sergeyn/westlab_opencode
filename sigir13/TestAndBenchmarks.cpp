/*
 * TestAndBenchmarks.cpp
 *
 *  Created on: Dec 10, 2012
 *      Author: sergeyn
 */

#include "TestAndBenchmarks.h"
#include "profiling.h"

TestAndBenchmarks::TestAndBenchmarks(std::vector<NewIteratorAugmented*>& lps, Options opts, const score_t thresh) :
lists(lps), threshold(thresh), options(opts){
}

void TestAndBenchmarks::measurePreprocessTime() { //of entire query -- for single term run on a one-term query log
	profilerC& profiler = profilerC::getTheInstance();
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;

	profiler.start(enm::PROC_AUGMENTS_ALL);
	for(unsigned i=0; i<=windows; ++i) {
		for(NewIteratorAugmented* list : lists) {
			list->preprocessAugmentsForWindow();
		}
		NewIteratorAugmented::incCurrentWindow();
	}
	COUT << profiler.end(enm::PROC_AUGMENTS_ALL)/1000 << ENDL;
}

//exhaustive
PriorityArray<QpResult<> > TestAndBenchmarks::doExhaustiveScan(const unsigned topK) { //of entire query -- for single term run on a one-term query log
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	PriorityArray<QpResult<> > resultsHeap(topK);
	//sortByDid(lists.begin(),lists.end());
	did_t smallestDid= lists[0]->did;
	score_t finalScore = 0;
	score_t lthreshold = threshold;
	score_t score=0.0f;

	// foreach window
	for(unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for(NewIteratorAugmented* list : lists)
			list->preprocessAugmentsForWindow();

		// compute Live Area and next window bound
		NewIteratorAugmented::buildLA_SSE(lthreshold,lists.size());
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) << options.DIDBLOCKBITS);

		// move all pointers to next live area, re-sort and set smallest did
		for (unsigned i=0; i<lists.size(); i++) {
			lists[i]->did = lists[i]->nextGEQ( smallestDid );
			profilerC::getTheInstance().stepCounter(enm::NEXTGEQ);
		}

		//COUTL << firstDidInWindow << " " << nextLiveDid << ENDL;
		//smallestDid = getSmallestDid(lists.begin(),lists.end());

		while(smallestDid<nextWinStart) {
			// initialize final score
			finalScore = 0.0f;

			// evaluate all dids with did == smallest_did
			for (unsigned i=0; i<lists.size(); ++i){
				if (lists[i]->did == smallestDid) {
					const float score = lists[i]->getScore();
					finalScore += score;
				}
			}

			// if calculated score more than threshold, heapify
			if (Fcompare(finalScore, lthreshold)==1) {
				//COUTL << final_score << " " << lthreshold << ENDL;
				resultsHeap.push(QpResult<>(smallestDid,finalScore));
				lthreshold = std::max(lthreshold,resultsHeap.head().score);
			}

			//smallestDid = NewIteratorAugmented::nextLiveArea(smallestDid+1);
			for (unsigned i=0; i<lists.size(); ++i){
				lists[i]->nextGEQ(smallestDid+1);
			}

			// sort list by did
			//sortByDid(lists.begin(),lists.end());
			smallestDid = lists[0]->did; 			// set new smallest did for the next round

		}
		NewIteratorAugmented::incCurrentWindow();
		if(score<0) throw;
	}
	return resultsHeap;
}

template <typename T>
T timeEvals() {
	T* arr = new T[1024*1024];
	profilerC::getTheInstance().start(enm::TEST1);
	T zero = 0;
	T acc = zero;
	for(unsigned c=0; c<19065; ++c) {
		acc = zero;
		for(unsigned i=1; i<=10; ++i ) {
			T sum = zero;
			for(unsigned terms = 0; terms<i; ++terms) {
				sum += arr[c*55+i+terms];
				//sum += CALC_SCORE(arr[c*55+i+terms],56,51.5); //emulate score calc.
				//sum += arr[c*55+i+terms]*42.3f; //emulate dequantization
			}
			acc += sum;
		}
	}
	COUTL << profilerC::getTheInstance().end(enm::TEST1) << ENDL;
	return acc;
}

void TestAndBenchmarks::timeFloatEvals() { timeEvals<float>(); }
void TestAndBenchmarks::timeIntEvals() { timeEvals<unsigned short>(); }
