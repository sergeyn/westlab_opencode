#ifndef INDEXHANDLER_H_
#define INDEXHANDLER_H_

#include "globals.h"

// The index is composed of compressed data chunks and metainfo describing them
// The meta is loaded from an sqlite database using the proxy.
// We do this as a batch when program starts and store the metainfo inside listIndexMetaData objects
// The actual compressed data is (currently) not part of sqlite and must be loaded from file system
// This is handled by onDemandCPool. Before using a compressed list we ask to materialize it
// Note: that if you want to prematerialize all terms prior to running them, make sure that
// the capacity of on demand pool is enough to store them all (for 1k quieries it is 2187), or else
// they are going to get evicted and the preloading efforts will be in vain.
//=============================================================================================
class onDemandCPool {
	vecUInt data;
	std::string termOrPath;

	static size_t capacity;			//how much terms we canh handle before evicting
	static size_t counter;		 	//how many terms we have preloaded
	static std::vector<onDemandCPool*> storage; //static storage for those
	static termsMap termsToInd;		//mapping terms to offset in static storage

public:
	static void initStaticPool(const unsigned cap);

	onDemandCPool()  {}
	~onDemandCPool() {} //no need to call evict upon destructor -- the vector will dealloc anyway

	void evictData();
	inline void setName(const std::string& t) { termOrPath = t; }
	inline const std::string& getName()  const { return termOrPath;}

	inline void setData(const unsigned int* d, size_t sz) { data = vecUInt(d,d+sz); }
	//the data must be materialized before using this operator!
	inline unsigned* getMaterializedData() {if(data.empty()) FATAL("data wasn't materialized!"); return &data[0]; }
	unsigned materialize( const std::vector<std::string>& peers);
	inline size_t getSize() { return data.size(); }
};
//=============================================================================================
struct listIndexMetaData {
	vecUInt maxDidPerBlock;					// the max value for each chunk
	vecUInt sizePerBlock;					// the compressed size for each chunk
	vecUInt flags;							// the pfor flag for each chunk -- Shuai!!!

	onDemandCPool compressedData;					// on-demand wrapper for the compressed data
	unsigned compressedDataLength;					// the length of compressedData
	unsigned metaInfoLength;						// the length of info
	unsigned unpaddedLength;
	std::string term;

	score_t maxScoreOfList;
	std::vector<score_t> DEPmaxScorePerBlock; // Depreciated because we move to did space block scores. but the index still might have those
};
//=============================================================================================
class termsIndexCache {
	std::vector<listIndexMetaData> data;

public:
	termsIndexCache() {} //doesn't make sense to have more than one of those...
	void addSingleToCache(const std::string& term);
	void fillCache(const termsMap& lex);
	void materializeAll();
	listIndexMetaData& operator[] (size_t i); //note: we might want to have the 'peers' version
	const std::string& getTerm(unsigned i) const { return data[i].term; }
	inline size_t size() const { return data.size(); }
};

class indexHandler {
public:
	termsIndexCache Cache;
	termsMap termsInCache;
	termsMap mappingForOnDemand;
	vecUInt documentLength;
	termsMap lex;

	void fillTermsMap(const std::string& path=CONSTS::INDEX::basic_table);
	void loadDocLengths(const std::string& fname=CONSTS::INDEX::doclenFileName);
public:

	indexHandler();
	listIndexMetaData& operator[] (const std::string& term); //note: this method might have eviction issues on small capacity (as it lacks the peers context)
	std::vector<listIndexMetaData*> operator[] (const std::vector<std::string>& terms);
	unsigned* getDocLensPtr() { return &documentLength[0]; }
};

#endif /* INDEXHANDLER_H_ */
