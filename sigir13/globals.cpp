/*
 * globals.cpp
 *
 *  Created on: Feb 17, 2012
 *      Author: sergeyn
 */



#include "globals.h"
#include <cmath>

unsigned globalVerbosity;

std::string CONSTS::INDEX::trecRoot("../trec06/");
std::string CONSTS::INDEX::doclenFileName(trecRoot + CONSTS::INDEX::DOCUMENT_LENGTH);
std::string CONSTS::INDEX::IndexTermMapping(trecRoot + "word_file");
std::string CONSTS::INDEX::sqlPath(trecRoot+"indx.sqlite");
std::string CONSTS::INDEX::basic_table(trecRoot+"basic_table");
std::string CONSTS::INDEX::TERM_PATH(trecRoot+"pool");
std::string CONSTS::INDEX::FLAG_PATH(trecRoot+"flag");
std::string CONSTS::INDEX::MAX_PATH(trecRoot+"max");
std::string CONSTS::INDEX::SCORE_PATH(trecRoot+"score");
std::string CONSTS::INDEX::SIZE_PATH(trecRoot+"size");

void CONSTS::INDEX::setTrecRoot(const std::string& path) {
	CONSTS::INDEX::trecRoot=path;
	CONSTS::INDEX::doclenFileName=(trecRoot + CONSTS::INDEX::DOCUMENT_LENGTH); // + CONSTS::INDEX::SORTED); // for reordered index run with -indexdir /research/constantinos/trec06_sorted/ and add after the document length (+ CONSTS::INDEX::SORTED)
	CONSTS::INDEX::IndexTermMapping=(trecRoot + "word_file");
	CONSTS::INDEX::sqlPath=(trecRoot+"indx.sqlite");
	CONSTS::INDEX::basic_table=(trecRoot+"basic_table");
	CONSTS::INDEX::TERM_PATH=(trecRoot+"pool");
	CONSTS::INDEX::FLAG_PATH=(trecRoot+"flag");
	CONSTS::INDEX::MAX_PATH=(trecRoot+"max");
	CONSTS::INDEX::SCORE_PATH=(trecRoot+"score");
	CONSTS::INDEX::SIZE_PATH=(trecRoot+"size");
}

//this is terrible and silly!
bool isNumber(const STRING& str) {
	if (str.size() == 0) return false;
	unsigned i = str[0]=='-'? 1 : 0;
	bool dotFound=false, digFound=false;
	for (; i<str.size(); ++i)
	{
		if (str[i]=='.') { if (dotFound) return false; dotFound=true; }
		else if (str[i]>'9' || str[i]<'0') return false;
		else digFound=true;
	}
	return digFound;
}
