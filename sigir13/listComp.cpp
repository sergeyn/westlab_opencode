#include "listComp.h"

int ListCompression::makeDirs(const std::string& rootPath) {
	//this unix specific booboo
	const std::string stringCmd = "mkdir -p " + 
		rootPath + "/pool/ " +
		rootPath + "/flag/ " +
		rootPath + "/max/ " +
		rootPath + "/size/ ";
	return system(stringCmd.c_str());
}

void ListCompression::serializeToFS(const std::string& rootPath ) {
	const std::string TERM_PATH = rootPath+"pool/";
	const std::string FLAG_PATH =  rootPath+"flag/";
	const std::string MAX_PATH = rootPath+"max/";
	const std::string SIZE_PATH = rootPath+"size/";

	dumpToFile(TERM_PATH+term,compressedVector.begin(),compressedVector.end()); // dump the compressed list
	dumpToFile(FLAG_PATH+term,flag.begin(),flag.end()); // dump the flags
	dumpToFile(SIZE_PATH+term,sizePerBlock.begin(),sizePerBlock.end()); // dump the size for each chunk
	dumpToFile(MAX_PATH+term,maxDidPerBlock.begin(),maxDidPerBlock.end()); //and the dids per block
}

void ListCompression::serializeToDb(const std::string& rootPath, SqlProxy& sql) {
	const std::string TERM_PATH = rootPath+"pool/";
	sqlite3_stmt *pStmt =  sql.prepare("insert or replace into terms values (?,?,?,?,?,?)"); // Compile the INSERT statement into a virtual machine.
	const size_t numb = lengthOfList/CONSTS::NUMERIC::BS;
	float dummy;
	sqlite3_bind_text(pStmt, 1, term.c_str(), -1, SQLITE_STATIC);
	sqlite3_bind_blob(pStmt, 2, &(flag[0]), numb*sizeof(unsigned int), SQLITE_STATIC);
	sqlite3_bind_blob(pStmt, 3, &(maxDidPerBlock[0]), numb*sizeof(unsigned int), SQLITE_STATIC);
	sqlite3_bind_blob(pStmt, 4, &(maxDidPerBlock[0]), 1, SQLITE_STATIC); //the min dummy
	sqlite3_bind_blob(pStmt, 5, &(maxScore), sizeof(float), SQLITE_STATIC);
	sqlite3_bind_blob(pStmt, 6, &(sizePerBlock[0]), numb*sizeof(unsigned int), SQLITE_STATIC);
	//COUT4 << "dump to file: " << term <<  " " <<  compressedVector.size() << " " << numb << " comp. ints" << ENDL;
	//dumpToFile(TERM_PATH+term,compressedVector.begin(),compressedVector.end()); // dump the compressed list
	sql.executeStoredStatement();
}

void ListCompression::compressDocsAndFreqs(const vecUInt& dids, const vecUInt& freqsOrQuants, float maxscore, bool isQuant) {
	maxScore = maxscore;
	//assuming BS padded list
	lengthOfList = dids.size();
	const unsigned BS(CONSTS::NUMERIC::BS);
	assert(dids.size()/BS == 0);
	assert(dids.size() == freqsOrQuants.size());
	
	size_t ind;
	maxDidPerBlock.reserve(lengthOfList/BS);
	//build delta list for doc ids
	vecUInt deltaList = dids;
	for (ind = lengthOfList-1; ind > 0; ind--) {
		deltaList[ind] -= deltaList[ind-1];
	}

	vecUInt freq = freqsOrQuants;
	if(!isQuant)
		for (ind = 0; ind < lengthOfList; ++ind)
			freq[ind]--;

	assert(lengthOfList == deltaList.size() && lengthOfList == freq.size());

	int j,flag1, flag2, numb;
	unsigned int *start, *ptrOfDelta, *ptrOfFreq;

	vecUInt buffer(lengthOfList*2);
	buffer.resize(lengthOfList*2);

	flag.clear();
	flag.reserve(lengthOfList/BS);
	sizePerBlock.clear();
	sizePerBlock.reserve(lengthOfList/BS);

	start = &buffer[0];

	//blocks iterator:
	for (size_t i = 0; i < lengthOfList; i += BS)	{
		COUTD << i << ENDL;
		vecUInt deltaTmp(deltaList.begin()+i,deltaList.begin()+i+BS); 
		vecUInt freqTmp(freq.begin()+i,freq.begin()+i+BS); 
		unsigned int* deltaPtr = &(deltaTmp[0]);
		unsigned int* freqPtr = &(freqTmp[0]);

		maxDidPerBlock.push_back(dids[i+BS-1]);
		flag1 = -1;
		for (j = 0; flag1 < 0; j++)	{
			ptrOfDelta = start;
			flag1 = pack_encode(&ptrOfDelta,deltaPtr, j);
		}

		flag2 = -1;
		for (j = 0; flag2 < 0; j++)	{
			ptrOfFreq = ptrOfDelta;
			flag2 = pack_encode(&ptrOfFreq, freqPtr, j);
		}

		flag.push_back(((unsigned int)(flag1))|(((unsigned int)(flag2))<<16));

		if (ptrOfFreq - start > 256)
			CERR << "One block is " << ptrOfFreq - start << " bytes" << ENDL;

		sizePerBlock.push_back( (unsigned char)((ptrOfFreq - start) - 1));
		numb++;
		start = ptrOfFreq;
	} //end for block
	compressedDataLength = start-&(buffer[0]);
	const size_t cells = compressedDataLength;//sizeof(unsigned int);
	compressedVector.reserve(cells);
	for(size_t i=0; i<cells; ++i)
		compressedVector.push_back(buffer[i]);

	COUT4 << " " << term << " lengthOfList: " << lengthOfList << " deltaList.size: " << deltaList.size() << " cpool_leng: " << compressedDataLength << ENDL;
}
