#ifndef NEWITERATOR_H_
#define NEWITERATOR_H_

#include "globals.h"
#include "utils.h"
#include "emmintrin.h"
#include "enums.h"

// integer pipeline
//#define USEINTPIPE
//#define USEQUANTINDEX
//#define USEUNCOMPRESSEDQUANTS

//#define USEREALSCORES
//#define USESORTEDINDEX
#define USEAUGMENTS

//basic list iterator.
//can be used for compressed lists,  raw lists, and quantized lists traversal
//note that for performance reasons we are always operating in a specific did range!

#define THREE 3
class NewIteratorBase {
public:
	did_t did; //the current did of this list. the deep state of the list in a way.
protected:
	unsigned offsetInPostingSpace;  //has different semantics under compressed and raw lists
	unsigned postingBlock;		     //blocks abstraction in posting space is used for skipping

	static unsigned* documentLengthArr; //the lengths of documents, used in bm25
	static unsigned  instancesCounter;

	//for onthefly guys this is always available
	did_t* realDids;			//someone kindly puts here dids
	

 	unsigned  paddedLength;         //number of postings in lists padded to be BS aligned

	//you are entering a world of pain -- the buffers must be aligned 16-bytes aligned for sse...
	unsigned  didsBuffer[CONSTS::NUMERIC::BS];	 	//temp buffer of size BS for decompressed dids of current posting block
	unsigned  freqsBuffer[CONSTS::NUMERIC::BS];		//temp buffer of size BS for decompressed freqs of current posting block

	//if real or quantized scores are used
//#ifdef USEQUANTINDEX
//	score_t* lookupForIndQuant;	//lookup table for dequantizing bytes to floats
//#endif
	#ifdef USEUNCOMPRESSEDQUANTS
		unchar* rawQuants; 
	#endif
	//pfor related stuff
	unsigned  offsetFromBase;
	unsigned  compressedBlock;
	unsigned* maxDidPerBlock;	// arr of max values for each chunk
	unsigned* sizePerBlock;		// arr of sizes for each chunk
	unsigned* flags;			// arr of pfor flags for each chunk -- Shuai!!!

	unsigned* ptrToFreqs;      //we obtain this ptr after decompressing a block of postings...

	unsigned* 	  compressedBase;	// pfor compressed chunks
	unsigned char fflag;		//indicator of freqsBuffer content and state
	unsigned char useReals;
	unsigned  postingsInWindow;


protected:
	inline unsigned  getFreq();	//return the freq. of this term in current posting (did)
	inline void 	 skipToDidBlockAndDecode(const did_t d); //populate the buffers with decompressed data of posting block containing d's nextGEQ
	inline unsigned* entireBlockDids(did_t* dids, const unsigned block) const; 

public:
	NewIteratorBase();
	static void setDocLenPtr(unsigned* ptr);
	//open list sets the compressed data related pointers
	void injectCompressedState(const unsigned unpaddedLen, unsigned *data, unsigned *sizes, unsigned* maxes, unsigned* flags);
	void setUseReals(bool use);
	inline unsigned	getPaddedLength() const;
	
	
	void resetPostingIterator();						//reset the list after traversal for new round
	inline did_t nextGEQ(const did_t d);				//changes state. returns did s.t. did>=d
	//inline score_t getScore();  //returns score for current did. this is either real score, result of score calc, or dequantization

	//inline static void sortByDid(NewIteratorBase** start, NewIteratorBase** end);
	//inline static did_t getSmallestDid(NewIteratorBase** start, NewIteratorBase** end);
};
//=============================================================================================
class NewIteratorFloatScore : public NewIteratorBase {
protected:
	score_t* realScores;            //and scores in this one
	score_t BM25Element;

public:
        inline score_t getScore();  //returns score for current did. this is either real score, result of score calc, or dequantization
        inline score_t        calcScore(const unsigned freq, const unsigned doclen) const; //BM25 calculation
        inline __m128         calcScore4(__m128 freq, __m128 len) const; //SSE BM25 calculation
        inline void   entireBlockDidsAndScores(did_t* dids, score_t* scores, const unsigned block) const; //decode given block and calc sco$
};
//=============================================================================================
class NewIteratorShortScore : public NewIteratorBase {
protected:
	scoreInt_t* realScores;            //and scores in this one

public:
        inline scoreInt_t getScore();  //returns score for current did. this is either real score, result of score calc, or dequantization
	template <int Mode>
        inline void   entireBlockDidsAndScores(did_t* dids, scoreInt_t* scores, const unsigned block) const; //decode given block and calc sco$
};
//=============================================================================================
struct PreprocessingData {
	score_t* 		blockMaxes;
	scoreInt_t* 	globalQuantBlMaxes;
	unchar* 		quantBlockMaxes;
	unsigned* 		postingBitSet;
	float 	 		quantile;
	unchar*		 	nonZeroBlockMaxes;
	unsigned* 		compressedZeros;
	unsigned* 		flagsForBMs;
	unsigned* 		sizesForBMs;
	unsigned* 		compressedBitset;
	unsigned* 		flagsForPBS;
	unsigned* 		sizesForPBS;
	unchar*			rawQuants;
	PreprocessingData();
};
//=============================================================================================
class NewIteratorAugmentedBase{
public:
	NewIteratorAugmentedBase();

#ifdef	USEAUGMENTS
public:

protected:
	unsigned* postingBitSet; //buffer for posting bitset of current window

	unsigned* bmQorC;  	  //ptr to quantized or compressed block maxes
	unsigned  scratchPadId;


	unsigned* postingBitSetBase; //for prestoring values of posting bitset

	unchar*	   nonZeroesBase;
	unsigned* sizesCompBMs;
	unsigned* flagsBM;
	ktuple<unsigned,4> stateForBMQC;

	enm::prprPolicy policy;	  //preprocessing policy indicator


	static unsigned  	didBlockBits;  //how many dids are considered a did space
	static unsigned* 	fakePostingBitSet;
	static unchar*		bitSetsPBS[16];
	static did_t*    	scratchPadDids[16];
	static unsigned* 	scratchPadQBMs[16];
	static unsigned* 	scratchPadPBS[16];
	static unsigned  	currentWindow;
	static unsigned  	firstDidOfWindow;
	//static unsigned* 	liveDeadArea; // moved to public,

public:
	static unsigned* 	liveDeadArea; //TOMOVE BACK

			void			initListFromCache(PreprocessingData& initData);
	#ifdef DEBUG
		PreprocessingData res;
	#endif
	inline void 			setScratchId(const unsigned id);
	inline unsigned 		getScratchId() const;
	inline void				setPolicy(enm::prprPolicy p);			//set the policy
	inline enm::prprPolicy	getPolicy() const;					    //get the policy
	inline did_t   			nextLivePosting(const did_t d) const ;  //uses internal pbs to get you next live posting miniblock
	inline did_t			getFirstDidOfNextBlock( const did_t d) const; // get the first did in the next block
	inline did_t	        getFirstDidinWindow() const; // get first did in the window

	static
	void			  		incCurrentWindow();
	static
	inline did_t   			nextLiveArea(const did_t d) ;			//find the next live area in this did's neighborhood (next set bit in bitset)
	
	static
	inline unsigned			getBits();						 		//get the bits of did space block
	static void				setBits(const unsigned bits);			//set the bits of did space block

	//static score_t 			debugGetBMForList(unsigned lst, unsigned block);

protected:
	template <typename T>
	inline void typedGenMaxesWithPtr(const did_t* docIds, const T* scores, T* blockMaxes, const unsigned size, const did_t smallest, const T assrtBound);
	template <typename Q>
	static unsigned 		compressQuantized(const Q* quantized, const unsigned sz, PreprocessingData& prData);
	static void			decompressQuantized(unsigned* dst, ktuple<unsigned,4>& offsets,
										const unchar* nonZeroBlockMaxes, unsigned* compressedZeros,
										const unsigned* flagsForBMs, unsigned* sizesForBMs, const unsigned countBlocks); //changed state: offsets in comp. data
	static unsigned 		compressPBS(const unsigned* pbs, const unsigned sz, PreprocessingData& prData);//todo

	static
	inline unsigned			getNextSetBit(const unsigned* ptr, const unsigned index);
#endif
};
//=============================================================================================
class NewIteratorAugmented : public NewIteratorAugmentedBase, public NewIteratorFloatScore 
{
	protected:
		
	score_t*   blockMaxes;	  //buffer for block maxes of current window
	score_t*   blockMaxesBase;	  //for prestoring values of block maxes
	score_t    quantileForBM; //unless onthefly BM scores are linearly quantized and need to be dequantized
	
	static score_t*  	scratchPadBMs[16];
	static score_t*   	scoresBuffer;		//temp buffer of size BS for decompressed freqs of current posting block
	static score_t*		blockScores[16];
	static score_t*  	scratchPadScores[16];

	
public:	
	NewIteratorAugmented();
	static NewIteratorAugmented scratchPads[16];
	score_t    maxScoreOfList;

	PreprocessingData 		buildInitialAugnemtsData(); 			//this is PREprocessing, not the inrpocessing
		   void				preprocessAugmentsForWindow();			//build the structures for current window	
	inline score_t 			getMaxScoreOfBlock(const did_t d) const;//gives you a score of a ds-block containing this did
	inline score_t 			getMaxScoreOfList() const; // get maxscore of list
	static void 			buildLA_SSE(const float threshold, const unsigned termsCount);
	static void 			initScratchpads();							//initialize statics
	
	void 					initBM25Float(const unsigned unpaddedLen);
	void 					initListFromCacheFloatPart(PreprocessingData& initData);

protected:
	void					handleAWindowOfOnTheFly(const did_t start, const did_t end);
	void					genMaxesWithPtr(const did_t* docIds, const score_t* scores, const unsigned size, const did_t smallest=0); //size is the amount of postings there
	static void				quantizeMaxes(const float* scores, const unsigned sz,  PreprocessingData& prData, const float maxScoreOfList=-1.0f);
};
//=============================================================================================
class NewIteratorIntAugmented : public NewIteratorAugmentedBase, public NewIteratorShortScore 
{
protected:
	scoreInt_t*   blockMaxes;	  //buffer for block maxes of current window
	scoreInt_t*   blockMaxesBase; //for prestoring values of block maxes
	scoreInt_t    quantileForBM;  //unless onthefly BM scores are linearly quantized and need to be dequantized
	
	static scoreInt_t*  	scratchPadBMs[16];
	static scoreInt_t*   	scoresBuffer;		//temp buffer of size BS for decompressed freqs of current posting block
	static scoreInt_t*		blockScores[16];
	static scoreInt_t*  	scratchPadScores[16];
	
public:	
	NewIteratorIntAugmented();
	static NewIteratorIntAugmented scratchPads[16];
	scoreInt_t    maxScoreOfList;

	PreprocessingData 		buildInitialAugnemtsData(); 			//this is PREprocessing, not the inrpocessing
		   void				preprocessAugmentsForWindow();			//build the structures for current window	
	inline scoreInt_t 		getMaxScoreOfBlock(const did_t d) const;//gives you a score of a ds-block containing this did
	inline scoreInt_t 		getMaxScoreOfList() const; 				// get maxscore of list
	static void				buildLA_SSE(const scoreInt_t threshold, const unsigned termsCount);
	static void 			initScratchpads();						//initialize statics
	void 					initListFromCacheIntPart(PreprocessingData& initData);

protected:
	void					handleAWindowOfOnTheFly(const did_t start, const did_t end);
	void					genMaxesWithPtr(const did_t* docIds, const scoreInt_t* scores, const unsigned size, const did_t smallest=0); //size is the amount of postings there
};
//=============================================================================================
	template<typename It, typename T>
	inline void popdown(unsigned ind);
	template<typename It, typename T>
	inline static void sortByDid(It start, It end);
	template<typename It, typename T>
	inline static void sortByMaxscore(It start, It end);	
	template<typename It>
	inline static did_t getSmallestDid(It start, It end);
	
void dequantLinear(const unsigned* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile);
void dequantLinear(const unchar* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile);
#include  "NewIteratorIMP.h"

#endif /* NEWITERATOR_H_ */

/*


When progr. starts:
   Init. all our scratchpads:
       allocate memory
       set the desired didBlockBits
       set document lengths ptr
       ...

Materialisation (either in qp loop or outside):
       inject compressed state
       decide on list policy using the oracle
       unless on-the-fly list build 'pre-stored' structures in main memory and inject them. this means:
       1) calc. scores and block maxes for the ENTIRE list and:
            a) store block maxes
            b) quantize block maxes and store
            c) quantize + compress and store
       2) construct pbs and:
            a) store it
            b) compress + store
            c) use fake


When we are at new window:
-------------------
For each on-the-fly list:
1) decompress their dids and freqs (in that window).
2) If we are dealing with quantized index:
   dequantize the scores hidden in freqs using lookup
   else: calculate the score.
3) next we build block maxes and posting bitsets
-------------------
For each prestored list:
1) obtain the block maxes by either of:
      a) move pointer to relevant location in materialized data O(1) //hard to believe we are going to use such policy
      b) dequantize the relevant chunk of block maxes O(n)
      c) decompress the compressed blockmaxes and then (b) O(n)+O(n)
2) obtain PBS by:
      a) moving a pointer O(1)
      b) decompressing pbs O(n)
-------------------
Now we use all the above to build our liveDeadArea bitset for the window.


*/

