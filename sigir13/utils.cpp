/*

 * utils.cpp
 *
 *  Created on: Oct 22, 2012
 *      Author: sergeyn
 */

#include "utils.h"
#include <cstring>
#include <time.h>

unsigned countSetBits(void* ptr, unsigned sizeInBytes) {
	unsigned *p = reinterpret_cast<unsigned*>(ptr);
	unsigned sum = 0;
	for(unsigned i=0; i<sizeInBytes/sizeof(unsigned); ++i)
		sum +=  __builtin_popcount(p[i]);
	return sum;
}

std::string getTimeString() {
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	return std::string(asctime(timeinfo));
}

int readline(FILE* fd, char* line) {
	if(fd==NULL) {
		CERR << "input file is wrong" << ENDL;
		exit(1);
	}

	char* tt;
	if(!feof(fd))	{
		memset(line,0,5120);
		if(fgets(line,5120,fd) == NULL) {
			//			CERR<<"end of file"<<endl;
			return -1;
		}

		if((tt=strchr(line,'\n')) == NULL) {
			CERR<<"No enter....... \n"<<ENDL;
			CERR<<"line is "<<line<<ENDL;
			exit(1);
		}
		*tt = ' ';
	}
	return 1;
}

// Usage: Statistics for the average index's Document Length
//why is it in utils?!
float avgDocumentLength() {
	FILE *handler = fopen(CONSTS::INDEX::doclenFileName.c_str(), "r");
	if (handler==NULL)
		CERR << CONSTS::INDEX::doclenFileName.c_str() << " could not be opened" << ENDL;

	int *doc = NEWM int[CONSTS::NUMERIC::MAXD];
	if( fread( doc, sizeof(int), CONSTS::NUMERIC::MAXD, handler) != CONSTS::NUMERIC::MAXD)
		CERR << "Document File: " << CONSTS::INDEX::doclenFileName << " does not contain " << CONSTS::NUMERIC::MAXD << " values " << ENDL;

	long long sum = 0;
	for (unsigned i=0; i<CONSTS::NUMERIC::MAXD; i++)
		sum += doc[i];

	fclose(handler);
	DEL(doc);
	float avg = (float) sum/CONSTS::NUMERIC::MAXD;
	COUT1 << "Total sum of document lengths: " << sum << " and the average document length: " << avg << ENDL;
	return avg;
}

// Load from the raw index the list lengths into a vector
// Input: nothing
// Output: vector of list lengths of all terms in our index
std::vector<int> loadEntireIndexListLengths() {
	std::vector<int> listLengths;
	std::string path = CONSTS::INDEX::trecRawRoot + CONSTS::INDEX::MERGED_BOOL_PATH + CONSTS::INDEX::INFO_INDEX;
	FILE* listLengthHandler = fopen(path.c_str(), "r");
	if (listLengthHandler==NULL)
		CERR << path << " could not be opened" << ENDL;

	int number_of_terms_in_collection;
	if(fread( &number_of_terms_in_collection, sizeof(int), 1, listLengthHandler)) {} // first int contains the # of terms in the entire collection

	unsigned int *inf_buffer = NEWM unsigned int[ 4 * number_of_terms_in_collection];
	if(fread( inf_buffer, sizeof(int), 4 * number_of_terms_in_collection, listLengthHandler)){}  // read all inf file

	for (int i=0; i<number_of_terms_in_collection; i++)
		listLengths.push_back(inf_buffer[i*4+1]); // the unpadded size is the second integer from the 4-tuple we load		fclose(List_Length_Handler);

	// print report
	COUT2<<"There are "<<number_of_terms_in_collection<<" terms in our index"<<ENDL;
	COUT2 << listLengths.size() << " distinct terms loaded in vector";
	return listLengths;
}

// Input: unordered map <string, int> lex
// Output: vector of list lengths of all terms in our loaded index
std::vector<int> loadLoadedIndexListLengths(termsMap& lex) {
	std::vector<int> List_Lengths;
	for (termsMap::iterator it = lex.begin(); it != lex.end(); ++it)
		List_Lengths.push_back(it->second);

	return List_Lengths;
}

// Usage: Get distinct terms' list length distribution from the Query log (1000 queries)
// Input: map containing terms and their unpadded list lengths
// Output: vector of int that contains all distinct's terms list length from the query log
//std::vector<int> loadQueryLogListLengths(termsMap& lex) {
//	// load query file
//	std::vector<int> List_Lengths;
//	termsMap queries_loaded;
//	int distinct_query_terms = 0;
//	FILE *query_log_handler = fopen(CONSTS::ikQuery.c_str(),"r");
//	while(1)  {
//		char line[5120];
//		if( readline(query_log_handler, line) == -1 )
//			break;
//
//		char *term;
//		term = strtok(line," ");
//
//		// First query term in line
//		std::unordered_map<std::string, int>::const_iterator term_exists = lex.find(std::string(term));
//		if ( term_exists != lex.end() ) { // if term exists
//			//std::unordered_map<std::string, int>::const_iterator query_exists = queries_loaded.find(std::string(term));
//			//if ( query_exists == queries_loaded.end() ) { // if term does not exists in our loaded queries
//			//	std::pair<std::string, int> new_entry (term_exists->first, term_exists->second);
//			//	queries_loaded.insert(new_entry);
//				List_Lengths.push_back(term_exists->second);
//			//	++distinct_query_terms;
//			//}
//		} else // term does not exist in our lexicon
//			CERR << std::string(term) << " was not found in lexicon" << ENDL;
//
//		// Remaining query terms in the same line
//		while ((term = strtok(NULL," ")) != NULL){
//			std::unordered_map<std::string, int>::const_iterator term1_exists = lex.find(std::string(term));
//			if ( term1_exists != lex.end() ) { // if term exists
//				//std::unordered_map<std::string, int>::const_iterator query_exists = queries_loaded.find(std::string(term));
//				//if ( query_exists == queries_loaded.end() ) { // if term does not exists in our loaded queries
//				//	std::pair<std::string, int> new_entry (term1_exists->first, term1_exists->second);
//				//	queries_loaded.insert(new_entry);
//					List_Lengths.push_back(term1_exists->second);
//				//	++distinct_query_terms;
//				//}
//			} else // term does not exist in our lexicon
//				CERR << std::string(term) << " was not found in lexicon" << ENDL;
//	  }
//	}
//	// close handler
//	fclose(query_log_handler);
//	COUT2 << "Total distinct terms in query trace that exist in our lexicon: " << distinct_query_terms <<ENDL;
//	return List_Lengths;
//}

// Print List length Distribution for the given list lengths
void printListLengthDistribution(std::vector<int>& listLengths) {
	std::vector<long> Buckets;
	// finer granularity observation of list length distribution
	for (int i=0; i<26; i++)
		Buckets.push_back(pow(2, i));

	std::vector<long long> listLengthBuckets (Buckets.size(), 0);

	long total_distinct_terms = 0;
	for (unsigned long i=0; i<listLengths.size(); i++) {
		for (unsigned  j=0; j<Buckets.size()-1; j++) {
			if ((listLengths.at(i) >= Buckets.at(j))&&(listLengths.at(i) < Buckets.at(j+1))) {
				listLengthBuckets.at(j) += 1;
				total_distinct_terms++;
				break;
			}
		}
	}

	// Print Results
	std::cout << "-----------------------------------------------------" << std::endl;
	std::cout << "Total number of distinct terms in the index: " << total_distinct_terms << std::endl;
	for (unsigned i=0; i<Buckets.size()-1; i++)
		std::cout << "# Lists with size [" << Buckets.at(i) << ", " << Buckets.at(i+1) << ") : " << listLengthBuckets.at(i) << std::endl;
	std::cout << "-----------------------------------------------------" << std::endl;
}

// Compare the Space requirements of BMW and MaxscoreHash
void spaceComparison(std::vector<int>& listLengths) {
	long long BMW_space = 0;
	unsigned long long Hash_space = 0;
	unsigned long long Hash_oracle_space = 0;
	long BMW_block_max = 64; // 1 float value for block max per 64 postings
	int Bucket_size = 19;
	long long GB_convertion = 1024*1024*1024;
	std::vector<unsigned long long> Bucket_Hash_Values (Bucket_size, 0);
	std::vector<int> Bucket (Bucket_size, 0);
	// new setting std::vector<int> Bucket (Bucket_size, 6);

///*  	// set parameters = FINAL,
	Bucket.at(0) = 10; // 2^7   -- 14
	Bucket.at(1) = 10; // 2^8   -- 14
	Bucket.at(2) = 10; // 2^9   -- 13
	Bucket.at(3) = 10; // 2^10  -- 12
	Bucket.at(4) = 6; // 2^11 --- 12
	Bucket.at(5) = 6; // 2^12 -- 10
	Bucket.at(6) = 7; // 2^13 -
	Bucket.at(7) = 7; // 2^14 --- 9
	Bucket.at(8) = 7; // 2^15 -- 8
	Bucket.at(9) = 8; // 2^16 - -----8
	Bucket.at(10) = 8; // 2^17 --- 8
	Bucket.at(11) = 7; // 2^18
	Bucket.at(12) = 6; // 2^19 -- was 7
	Bucket.at(13) = 6; // 2^20 -- was 7
	Bucket.at(14) = 6; // 2^21 -- was 7
	Bucket.at(15) = 6; // 2^22 -- was 7
	Bucket.at(16) = 6; // 2^23 -- was 7
	Bucket.at(17) = 6; // 2^24 -- was 7

	std::cout << "########## Parameter List #############" << std::endl;
	// print out parameter list
	for (unsigned int i=0; i<Bucket.size(); i++)
		std::cout << Bucket.at(i) << std::endl;
	std::cout << "#################" << std::endl;

	// for each list loaded
	for (unsigned i=0; i<listLengths.size(); i++) {
		// compute BMW space (both BMW_space are working but the second is more elegant)
		//BMW_space += (List_Lengths.at(i)%BMW_block_max == 0) ? (int) List_Lengths.at(i)/BMW_block_max : (int) List_Lengths.at(i)/BMW_block_max + 1;
		BMW_space += 1 + (( listLengths.at(i) - 1 ) / BMW_block_max );

		// compute Hash space
		unsigned int lenBits = intlog2(listLengths.at(i));
		int Hash_block_max = lenBits < 8 ? 10 : 6;
		float Hash_blocksize = pow(2, (float) Hash_block_max);
		float Total = pow(2, (float) 25);
		Hash_space += 1 +  (( Total - 1 ) /  Hash_blocksize );
/*
		// togo Expected postings per block space computation
		unsigned int expectedBits = 6;
		unsigned int maxdBits = CONSTS::NUMERIC::MAXDBITS;
		unsigned int bbits = (maxdBits-lenBits+expectedBits);
		float blocksize = pow(2, (float)bbits);
		Hash_oracle_space += 1 + (( CONSTS::NUMERIC::MAXD - 1 ) / blocksize );
*/
		unsigned int bbits;
		int B_counter = 0;
		for (unsigned i=7; i<=25; i++) {
			if (lenBits<i) {
				bbits = Bucket.at(B_counter);
				float blocksize = pow(2, (float) bbits);
				Hash_oracle_space += 1 + (( CONSTS::NUMERIC::MAXD - 1 ) / blocksize );
				Bucket_Hash_Values.at(B_counter) += 1 + (( CONSTS::NUMERIC::MAXD - 1 ) / blocksize ); // to change maxd ?
				break;
			} else
				++B_counter;
		}
	}

	long long BMW_size = BMW_space*sizeof(float);
	long long Hash_size = Hash_space*sizeof(float);
	unsigned long long Hash_size_oracle = Hash_oracle_space*sizeof(float);

	double BMW_GB = (double) BMW_size/GB_convertion;
	double Hash_GB = (double) Hash_size/GB_convertion;
	double Hash_oracle_GB = (double) Hash_size_oracle/GB_convertion;

	// Print Results
	std::cout << "-----------------------------------------------------" << std::endl;
	std::cout << "Total number of distinct terms: " << listLengths.size() << std::endl;
	std::cout << "BMW space: " << BMW_space << " block maxes values" << std::endl;
	std::cout << BMW_size << " bytes total space and " << BMW_GB << " GB of space" << std::endl;
	std::cout << "-----------------------------------------------------" << std::endl;
	std::cout << "Hash space (2^6): " << Hash_space << " block maxes values" << std::endl;
	std::cout << Hash_size << " bytes total space and " << Hash_GB << " GB of space" << std::endl;
	std::cout << "-----------------------------------------------------" << std::endl;
	std::cout << "(if > billions not reliable) Hash space (oracle): " << Hash_oracle_space << " block maxes values" << std::endl;
	std::cout << Hash_size_oracle << " bytes total space and " << Hash_oracle_GB << " GB of space" << std::endl;
	std::cout << "-----------------------------------------------------" << std::endl;

	// # bytes
	std::vector<unsigned long long> Bucket_Hash_Bytes(Bucket_size, 0);
	std::vector<double> Bucket_Hash_GBs(Bucket_size, 0);
	double total = 0.0f;

	// print bucketized results
	for (unsigned i=0; i<Bucket_Hash_Values.size(); i++) {
		Bucket_Hash_Bytes.at(i) = Bucket_Hash_Values.at(i)*sizeof(float);
		Bucket_Hash_GBs.at(i) = (double) Bucket_Hash_Bytes.at(i)/GB_convertion;
		total+= Bucket_Hash_GBs.at(i);
		std::cout << "Bucket (" << i << ") \t (2^" << (i+7) << ")\t# block-max values: " << Bucket_Hash_Values.at(i) << "\t # bytes: " << Bucket_Hash_Bytes.at(i) << "\tspace: " << Bucket_Hash_GBs.at(i) << " GBs " << std::endl;
	}
	std::cout << "-----------------------------------------------------" << std::endl;
	std::cout << "Total reliable space in GBs: " << total << std::endl;
}
