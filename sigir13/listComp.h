#ifndef _LISTCOMP_H_
#define _LISTCOMP_H_

#include <vector>
#include <algorithm>
#include <iterator>

#include "SqlProxy.h"
#include "pfor.h"
#include "utils.h"
#include "globals.h"
#include "sql/sqlite3.h"

class ListCompression  {
public:
	vecUInt maxDidPerBlock;					// the max value for each comp. chunk
	vecUInt sizePerBlock;					// the size for each comp. chunk
	vecUInt flag;							// the flag for each comp. chunk -- Shuai!!!
	vecUInt compressedVector;				// the compressed data

	unsigned compressedDataLength;					// the length of compressedData - sum of sizePerBlock
	unsigned metaInfoLength;						// the length of meta info

	std::string term;
	unsigned lengthOfList;
	
	float maxScore;
	
	ListCompression(const std::string& trm) : term(trm) {}
	void compressDocsAndFreqs(const vecUInt& dids, const vecUInt& freqsOrQuants, float maxscore, bool isQuant);
	void serializeToFS(const std::string& rootPath);
	void serializeToDb(const std::string& rootPath, SqlProxy&);
	static int makeDirs(const std::string& rootPath);
};
#endif
