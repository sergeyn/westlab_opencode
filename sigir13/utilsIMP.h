/*
 * utils.h
 *
 *  Created on: Mar 22, 2012
 *      Author: sergeyn
 */

#ifndef UTILS_H_IM
#define UTILS_H_IM

// Knuth trick for comparing floating numbers
inline bool FloatEquality( float a, float b) { // check if a and b are equal with respect to the defined tolerance epsilon
	//const float epsilon = 0.0001; // or some other small number (for our case it is sufficient)
	if ((std::fabs(a-b) <= CONSTS::NUMERIC::EPSILON*std::fabs(a)) && (std::fabs(a-b) <= CONSTS::NUMERIC::EPSILON*std::fabs(b))) return true;
	else return false; }

inline bool FloatComparisonPrivate( float a, float b) { // compare two floats ONLY for greater or smaller and NOT for equality
	if (a > b) return true;
	else return false; }

// Arguments a,b and returns a) 0 in case of equality, b) 1 in case a > b, and c) -1 in case a < b
inline int Fcompare( float a, float b) {
	if (FloatEquality(a, b)) return 0; // equal
	else { if (FloatComparisonPrivate(a, b)) return 1;  // a > b
	else return -1; }      // a < b
}

/*
	inline void Get_Percentiles(lptrArray& lps, std::vector<float>& percentiles, const float& threshold) {
		const float frequency = lps[i]->getFreq();
		const float score = lps[i]->calcScore(frequency,pages[smallest_did]);
	}
 */

inline uint32_t intlog2(const uint32_t x) {
	uint32_t y;
	asm ( "\tbsr %1, %0\n"
			: "=r"(y)
			  : "r" (x)
	);
	return y;
}

inline unsigned int NearestPowerOf2( unsigned int x ){ //test this first...
	return 1 << (1+intlog2(x));
}

template <typename T> //stream vectors
std::ostream& operator<< (std::ostream& s, const std::vector<T>& t) {
	for(auto& it : t)
		s << it << ' ';
	return s;
}

template<typename T>
PriorityArray<T>::PriorityArray(const size_t sz):size(sz){
	minData.resize(size+1);
	//std::fill(minData.begin(),minData.end(),std::numeric_limits<T>::min());
}

template<typename T>
inline const std::vector<T>& PriorityArray<T>::getV() const { return minData;}

template<typename T>
inline void PriorityArray<T>::putInArray(T* ptr) {sortData(); for(unsigned i=0; i<minData.size(); ++i) ptr[i]=minData[i]; }

template<typename T>
inline const T& PriorityArray<T>::head() const { return minData[0]; }

template<typename T>
inline void PriorityArray<T>::sortData() { std::sort(minData.begin(), minData.end()); std::reverse(minData.begin(), minData.end()); }

template<typename T>
inline bool PriorityArray<T>::push(const T& val){
	//are we better than the smallest top-k member?
	if(minData[0]>val)
		return false;
	minData[0] = val;
	for (unsigned int i = 0; (2*i+1) < size; ){
		unsigned int next = 2*i + 1;
		if (minData[next] > minData[next+1])
			++next;
		if (minData[i] > minData[next])
			std::swap(minData[i], minData[next]);
		else
			break;
		i = next;
	}
	return true;
}


template <typename T>
inline T max3(const T& a, const T& b, const T& c) {
	if(a<b) return (b<c) ? c : b;
	else return (a<c) ? c : a;
}

template <typename T>
inline T min3(const T& a, const T& b, const T& c) {
	if(a>b) return (b>c) ? c : b;
	else return (a>c) ? c : a;
}

inline unsigned offsetInAlignedForSSE(void* ptr) { //bytes
	unsigned long long tmp = (unsigned long long)ptr;
	return (tmp%16);
}

inline bool isAlignedForSSE(void* ptr) {
	return offsetInAlignedForSSE(ptr)==0;
}

inline void NEWMA(void** ptr,const unsigned bytecounts) {
	char *ptrT = new char[bytecounts];
	ptr[0] = (void*) ptrT;
	//if(posix_memalign(ptr,128,bytecounts)) FATAL("NEWMA failed");
	//allocatedSum += bytecounts;
	//COUTL << " allocated " << bytecounts << " total: " << allocatedSum << ENDL;
}

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
template<typename _FIter, typename _Tp>
_FIter gallop_up_lower_bound(_FIter begin, _FIter end, const _Tp& val) {
	assert(*(end-1) >= val); //the search must end -- rowinski is a problem again
	unsigned skip = 1;
	while(begin<end) {
		if(val == *begin)	return begin;
		else if(*begin < val) {
			begin = begin+skip;
			skip <<=1;
		}
		else //the last skip was too much
			return std::lower_bound(begin-(skip>>1),begin,val);
	}
	//the last skip was way too much
	return std::lower_bound(begin-(skip>>1),end,val);
}

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
//this is not the same convention as std:: version -- we need it for nextGEQing
template<typename _FIter, typename _Tp>
_FIter gallop_down_lower_bound(_FIter begin, _FIter curr, const _Tp& val) {
	assert(*(curr) > val); //the search must end
	unsigned skip = 1;
	while(begin<curr) {
		if(val == *curr)	return curr;
		else if(*curr > val) {
			curr = curr-skip;
			skip <<=1;
		}
		else //the last skip was too much
			return std::lower_bound(curr,curr+(skip>>1),val);
	}
	//the last skip was way too much
	return std::lower_bound(begin,curr+(skip>>1),val);
}

template <typename Iterator> //make ofstream version, really
void dumpToFile(const std::string& path, Iterator start, Iterator end) {
	FILE *fdo = fopen(path.c_str(),"w");
	if(! fdo) FATAL("Failed writing to " + path );
	for (; start !=end; ++start)
		if (fwrite(&(*start), sizeof(typename Iterator::value_type), 1, fdo) != 1)
			FATAL("Failed writing to " + path );
	fclose(fdo);
}
#endif /* UTILS_H_ */
