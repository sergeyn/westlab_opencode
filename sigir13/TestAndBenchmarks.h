#ifndef TESTANDBENCHMARKS_H_
#define TESTANDBENCHMARKS_H_

#include "NewIterator.h"
#include "options.h"

class TestAndBenchmarks {
private:
	std::vector<NewIteratorAugmented*> lists;
	const score_t threshold;
	Options options;

public:
	TestAndBenchmarks(std::vector<NewIteratorAugmented*>& lps, Options opts, const score_t thresh=0.0f);

	void measurePreprocessTime();
	PriorityArray<QpResult<> >  doExhaustiveScan(const unsigned topk);

	void timeFloatEvals();
	void timeIntEvals();

};

#endif /* TESTANDBENCHMARKS_H_ */
