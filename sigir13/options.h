/* #This file was generated automatically! Do not edit */
#ifndef _WEST_OPTIONS_
#define _WEST_OPTIONS_

#include "globals.h"

struct OptionRanges
{
	unsigned DIDBLOCKBITS_MINVAL, DIDBLOCKBITS_MAXVAL;
	unsigned VERBOSITY_MINVAL, VERBOSITY_MAXVAL;
	unsigned KTERMSBUCKET_MINVAL, KTERMSBUCKET_MAXVAL;
	unsigned MORETHANKTERMS_MINVAL, MORETHANKTERMS_MAXVAL;
	unsigned LIMIT_MINVAL, LIMIT_MAXVAL;
	unsigned TOPK_MINVAL, TOPK_MAXVAL;
	unsigned LAYER_MINVAL, LAYER_MAXVAL;
	unsigned ONTHEFLYSTOPAT_MINVAL, ONTHEFLYSTOPAT_MAXVAL;
	unsigned SKIPTOQUERY_MINVAL, SKIPTOQUERY_MAXVAL;
	unsigned CLRVTPERCENT_MINVAL, CLRVTPERCENT_MAXVAL;
	unsigned PRINTALLRES_MINVAL, PRINTALLRES_MAXVAL;
	unsigned IMBUE_MINVAL, IMBUE_MAXVAL;
	STRING GOLDENSCORES_MINVAL, GOLDENSCORES_MAXVAL;
	STRING QLOG_MINVAL, QLOG_MAXVAL;
	STRING INDEXDIR_MINVAL, INDEXDIR_MAXVAL;
	STRING POLICY_MINVAL, POLICY_MAXVAL;
	unsigned countAllOpts;
	OptionRanges() : 
		DIDBLOCKBITS_MINVAL(1), DIDBLOCKBITS_MAXVAL(26), //unsigned
		VERBOSITY_MINVAL(0), VERBOSITY_MAXVAL(10), //unsigned
		KTERMSBUCKET_MINVAL(0), KTERMSBUCKET_MAXVAL(20), //unsigned
		MORETHANKTERMS_MINVAL(0), MORETHANKTERMS_MAXVAL(20), //unsigned
		LIMIT_MINVAL(0), LIMIT_MAXVAL(2000000), //unsigned
		TOPK_MINVAL(1), TOPK_MAXVAL(100000), //unsigned
		LAYER_MINVAL(0), LAYER_MAXVAL(100), //unsigned
		ONTHEFLYSTOPAT_MINVAL(1), ONTHEFLYSTOPAT_MAXVAL(25), //unsigned
		SKIPTOQUERY_MINVAL(0), SKIPTOQUERY_MAXVAL(10000), //unsigned
		CLRVTPERCENT_MINVAL(0), CLRVTPERCENT_MAXVAL(100), //unsigned
		PRINTALLRES_MINVAL(0), PRINTALLRES_MAXVAL(2), //unsigned
		IMBUE_MINVAL(0), IMBUE_MAXVAL(1), //unsigned
		GOLDENSCORES_MINVAL(), GOLDENSCORES_MAXVAL(), //STRING
		QLOG_MINVAL(), QLOG_MAXVAL(), //STRING
		INDEXDIR_MINVAL(), INDEXDIR_MAXVAL(), //STRING
		POLICY_MINVAL(), POLICY_MAXVAL(), //STRING
		countAllOpts(16)
	{}
};

struct Options
{
	
	unsigned DIDBLOCKBITS;
	unsigned VERBOSITY;
	unsigned KTERMSBUCKET;
	unsigned MORETHANKTERMS;
	unsigned LIMIT;
	unsigned TOPK;
	unsigned LAYER;
	unsigned ONTHEFLYSTOPAT;
	unsigned SKIPTOQUERY;
	unsigned CLRVTPERCENT;
	unsigned PRINTALLRES;
	unsigned IMBUE;
	STRING GOLDENSCORES;
	STRING QLOG;
	STRING INDEXDIR;
	STRING POLICY;

	unsigned countAllOpts;
	OptionRanges	valueRanges;

	Options() : 
		DIDBLOCKBITS(6), //unsigned
		VERBOSITY(3), //unsigned
		KTERMSBUCKET(0), //unsigned
		MORETHANKTERMS(1), //unsigned
		LIMIT(2000000), //unsigned
		TOPK(10), //unsigned
		LAYER(0), //unsigned
		ONTHEFLYSTOPAT(18), //unsigned
		SKIPTOQUERY(0), //unsigned
		CLRVTPERCENT(70), //unsigned
		PRINTALLRES(0), //unsigned
		IMBUE(1), //unsigned
		GOLDENSCORES("/research/index/QueryLog/Golden_Threshold_trec06"), //STRING
		QLOG("/research/index/QueryLog/1000query"), //STRING
		INDEXDIR("/research/index/trec06/"), //STRING
		POLICY(""), //STRING
		countAllOpts(16)
	{}

	unsigned signatureF(const unsigned& value)	{ return value; }
	unsigned signatureF(const float& value)		{ return unsigned(1000000*value); }
	unsigned signatureF(const double& value)	{ return unsigned(1000000*value); }
	unsigned signatureF(const STRING& value)	{ return 0; }

	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, STRING& target)	{ if(argName == name) { target=argVal; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, unsigned& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, double& target)	{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; } 
	bool convertArg(const STRING& argName, const STRING& argVal, const STRING& name, float& target)		{ if(argName == name) { STRINGSTREAM s(argVal); s >> target; return true;} return false; }

	bool parseArg(const STRING& argName, const STRING& argVal)
	{
		if (convertArg(argName, argVal, STRING("-didblockbits"), DIDBLOCKBITS)) return true;
		if (convertArg(argName, argVal, STRING("-verbosity"), VERBOSITY)) return true;
		if (convertArg(argName, argVal, STRING("-ktermsbucket"), KTERMSBUCKET)) return true;
		if (convertArg(argName, argVal, STRING("-morethankterms"), MORETHANKTERMS)) return true;
		if (convertArg(argName, argVal, STRING("-limit"), LIMIT)) return true;
		if (convertArg(argName, argVal, STRING("-topk"), TOPK)) return true;
		if (convertArg(argName, argVal, STRING("-layer"), LAYER)) return true;
		if (convertArg(argName, argVal, STRING("-ontheflystopat"), ONTHEFLYSTOPAT)) return true;
		if (convertArg(argName, argVal, STRING("-skiptoquery"), SKIPTOQUERY)) return true;
		if (convertArg(argName, argVal, STRING("-clrvtpercent"), CLRVTPERCENT)) return true;
		if (convertArg(argName, argVal, STRING("-printallres"), PRINTALLRES)) return true;
		if (convertArg(argName, argVal, STRING("-imbue"), IMBUE)) return true;
		if (convertArg(argName, argVal, STRING("-goldenscores"), GOLDENSCORES)) return true;
		if (convertArg(argName, argVal, STRING("-qlog"), QLOG)) return true;
		if (convertArg(argName, argVal, STRING("-indexdir"), INDEXDIR)) return true;
		if (convertArg(argName, argVal, STRING("-policy"), POLICY)) return true;
		return false;
	}

	STRING getVal(const STRING& optName)
	{
		if (optName == STRING("-didblockbits")) return toString(DIDBLOCKBITS);
		if (optName == STRING("-verbosity")) return toString(VERBOSITY);
		if (optName == STRING("-ktermsbucket")) return toString(KTERMSBUCKET);
		if (optName == STRING("-morethankterms")) return toString(MORETHANKTERMS);
		if (optName == STRING("-limit")) return toString(LIMIT);
		if (optName == STRING("-topk")) return toString(TOPK);
		if (optName == STRING("-layer")) return toString(LAYER);
		if (optName == STRING("-ontheflystopat")) return toString(ONTHEFLYSTOPAT);
		if (optName == STRING("-skiptoquery")) return toString(SKIPTOQUERY);
		if (optName == STRING("-clrvtpercent")) return toString(CLRVTPERCENT);
		if (optName == STRING("-printallres")) return toString(PRINTALLRES);
		if (optName == STRING("-imbue")) return toString(IMBUE);
		if (optName == STRING("-goldenscores")) return toString(GOLDENSCORES);
		if (optName == STRING("-qlog")) return toString(QLOG);
		if (optName == STRING("-indexdir")) return toString(INDEXDIR);
		if (optName == STRING("-policy")) return toString(POLICY);
		CERR << "Unknown option: " << optName << ENDL; exit(1);
		return "";
	}

	void parseFromCmdLine(int argc, char** argv, const int id=-99999)
	{
		const STRING idSuffix(":"+ toString(id));

		for (int i=0; i<argc; ++i)
		{
			STRING argvi(argv[i]);
			if (argvi[0] != '-')
			{
				CERR << "Unexpected argument: " << argvi << ENDL; goto _PrintUsage; 
			}
			else if (argc <= i+1)
			{
				CERR << "Value missing for argument: " << argvi << ENDL; goto _PrintUsage;
			}
			// for all other args, the val should be numeric
			//else if (!isNumber(argv[i+1]))
			//{
			//	CERR << "Unexpected value " << argv[i+1] << " for argument: " << argv[i] << ENDL; goto _PrintUsage;
			//}
			else if (argvi.find(':') != STRING::npos)  // id specific opt
			{
				size_t s = argvi.find(idSuffix);
				if(s == STRING::npos) { ++i; continue; } // not of this id
				argvi.erase(s,argvi.size()-s);
				if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; }
			}
			else if (parseArg(argvi, STRING(argv[i+1]))) { ++i; continue; } // regular opt

			CERR << "Unknown option: " << argv[i] << ENDL;
			
			_PrintUsage:
			CERR << "Usage:" << ENDL
			 << "	-didblockbits <val=6> // " << "The did space blocks will be of size 1<<bits" << ENDL
			 << "	-verbosity <val=3> // " << "The verbosity level (higher is more)" << ENDL
			 << "	-ktermsbucket <val=0> // " << "Run only queries with exactly k terms. Default is 0 -- run all" << ENDL
			 << "	-morethankterms <val=1> // " << "Run only queries with more than k terms. Default is 1 -- don't run singles" << ENDL
			 << "	-limit <val=2000000> // " << "Run only n queries. Default is -- run all" << ENDL
			 << "	-topk <val=10> // " << "Run top-k. Default is 10" << ENDL
			 << "	-layer <val=0> // " << "Layer" << ENDL
			 << "	-ontheflystopat <val=18> // " << "We do on the fly preproc. for lists shorter than 2^k" << ENDL
			 << "	-skiptoquery <val=0> // " << "Layer" << ENDL
			 << "	-clrvtpercent <val=70> // " << "this/100*threshold" << ENDL
			 << "	-printallres <val=0> // " << "0 - print only if worng score, 1 -- all, >1 -- none" << ENDL
			 << "	-imbue <val=1> // " << "if 1 will print all numbers with thousands separator" << ENDL
			 << "	-goldenscores <val=/research/index/QueryLog/Golden_Threshold_trec06> // " << "this files holds the golden scores" << ENDL
			 << "	-qlog <val=/research/index/QueryLog/1000query> // " << "query log of choice" << ENDL
			 << "	-indexdir <val=/research/index/trec06/> // " << "your index is there!" << ENDL
			 << "	-policy <val=> // " << "file for overriding static policies in womanly fashion default: empty" << ENDL
			<< ENDL;

			exit(1);
		}
	}

	bool testAllOptions() const 
	{
		if (DIDBLOCKBITS < valueRanges.DIDBLOCKBITS_MINVAL || DIDBLOCKBITS > valueRanges.DIDBLOCKBITS_MAXVAL) { CERR<<  " DIDBLOCKBITS is out of range: [1,26]" << ENDL; return false;}
		if (VERBOSITY < valueRanges.VERBOSITY_MINVAL || VERBOSITY > valueRanges.VERBOSITY_MAXVAL) { CERR<<  " VERBOSITY is out of range: [0,10]" << ENDL; return false;}
		if (KTERMSBUCKET < valueRanges.KTERMSBUCKET_MINVAL || KTERMSBUCKET > valueRanges.KTERMSBUCKET_MAXVAL) { CERR<<  " KTERMSBUCKET is out of range: [0,20]" << ENDL; return false;}
		if (MORETHANKTERMS < valueRanges.MORETHANKTERMS_MINVAL || MORETHANKTERMS > valueRanges.MORETHANKTERMS_MAXVAL) { CERR<<  " MORETHANKTERMS is out of range: [0,20]" << ENDL; return false;}
		if (LIMIT < valueRanges.LIMIT_MINVAL || LIMIT > valueRanges.LIMIT_MAXVAL) { CERR<<  " LIMIT is out of range: [0,2000000]" << ENDL; return false;}
		if (TOPK < valueRanges.TOPK_MINVAL || TOPK > valueRanges.TOPK_MAXVAL) { CERR<<  " TOPK is out of range: [1,100000]" << ENDL; return false;}
		if (LAYER < valueRanges.LAYER_MINVAL || LAYER > valueRanges.LAYER_MAXVAL) { CERR<<  " LAYER is out of range: [0,100]" << ENDL; return false;}
		if (ONTHEFLYSTOPAT < valueRanges.ONTHEFLYSTOPAT_MINVAL || ONTHEFLYSTOPAT > valueRanges.ONTHEFLYSTOPAT_MAXVAL) { CERR<<  " ONTHEFLYSTOPAT is out of range: [1,25]" << ENDL; return false;}
		if (SKIPTOQUERY < valueRanges.SKIPTOQUERY_MINVAL || SKIPTOQUERY > valueRanges.SKIPTOQUERY_MAXVAL) { CERR<<  " SKIPTOQUERY is out of range: [0,10000]" << ENDL; return false;}
		if (CLRVTPERCENT < valueRanges.CLRVTPERCENT_MINVAL || CLRVTPERCENT > valueRanges.CLRVTPERCENT_MAXVAL) { CERR<<  " CLRVTPERCENT is out of range: [0,100]" << ENDL; return false;}
		if (PRINTALLRES < valueRanges.PRINTALLRES_MINVAL || PRINTALLRES > valueRanges.PRINTALLRES_MAXVAL) { CERR<<  " PRINTALLRES is out of range: [0,2]" << ENDL; return false;}
		if (IMBUE < valueRanges.IMBUE_MINVAL || IMBUE > valueRanges.IMBUE_MAXVAL) { CERR<<  " IMBUE is out of range: [0,1]" << ENDL; return false;}
		if (GOLDENSCORES < valueRanges.GOLDENSCORES_MINVAL || GOLDENSCORES > valueRanges.GOLDENSCORES_MAXVAL) { CERR<<  " GOLDENSCORES is out of range: [,]" << ENDL; return false;}
		if (QLOG < valueRanges.QLOG_MINVAL || QLOG > valueRanges.QLOG_MAXVAL) { CERR<<  " QLOG is out of range: [,]" << ENDL; return false;}
		if (INDEXDIR < valueRanges.INDEXDIR_MINVAL || INDEXDIR > valueRanges.INDEXDIR_MAXVAL) { CERR<<  " INDEXDIR is out of range: [,]" << ENDL; return false;}
		if (POLICY < valueRanges.POLICY_MINVAL || POLICY > valueRanges.POLICY_MAXVAL) { CERR<<  " POLICY is out of range: [,]" << ENDL; return false;}
		return true;
	}

	template<class B> void print(B& pOut)
	{
		pOut << "----------------------------" << SLASHN;
		#define PRINT_OPT(external_name, internal_name) { pOut << external_name  << " = " << toString(internal_name) << SLASHN; }
		PRINT_OPT("-didblockbits", DIDBLOCKBITS);
		PRINT_OPT("-verbosity", VERBOSITY);
		PRINT_OPT("-ktermsbucket", KTERMSBUCKET);
		PRINT_OPT("-morethankterms", MORETHANKTERMS);
		PRINT_OPT("-limit", LIMIT);
		PRINT_OPT("-topk", TOPK);
		PRINT_OPT("-layer", LAYER);
		PRINT_OPT("-ontheflystopat", ONTHEFLYSTOPAT);
		PRINT_OPT("-skiptoquery", SKIPTOQUERY);
		PRINT_OPT("-clrvtpercent", CLRVTPERCENT);
		PRINT_OPT("-printallres", PRINTALLRES);
		PRINT_OPT("-imbue", IMBUE);
		PRINT_OPT("-goldenscores", GOLDENSCORES);
		PRINT_OPT("-qlog", QLOG);
		PRINT_OPT("-indexdir", INDEXDIR);
		PRINT_OPT("-policy", POLICY);
		pOut << "----------------------------" << ENDL;
	}
	
	template<class B> void printCSV(B& pOut)
	{
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	unsigned signature()
	{
		unsigned res = 0;
		res ^= (rand() * signatureF(DIDBLOCKBITS));
		res ^= (rand() * signatureF(VERBOSITY));
		res ^= (rand() * signatureF(KTERMSBUCKET));
		res ^= (rand() * signatureF(MORETHANKTERMS));
		res ^= (rand() * signatureF(LIMIT));
		res ^= (rand() * signatureF(TOPK));
		res ^= (rand() * signatureF(LAYER));
		res ^= (rand() * signatureF(ONTHEFLYSTOPAT));
		res ^= (rand() * signatureF(SKIPTOQUERY));
		res ^= (rand() * signatureF(CLRVTPERCENT));
		res ^= (rand() * signatureF(PRINTALLRES));
		res ^= (rand() * signatureF(IMBUE));
		res ^= (rand() * signatureF(GOLDENSCORES));
		res ^= (rand() * signatureF(QLOG));
		res ^= (rand() * signatureF(INDEXDIR));
		res ^= (rand() * signatureF(POLICY));
		return res;
	}
};

#endif
