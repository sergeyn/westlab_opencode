#include <sstream>
#include <fstream>
#include <iterator>
#include <cstring>
#include <iomanip>

#include "Executor.h"
#include "utils.h"

#include "TestAndBenchmarks.h"
#include "OR.h"
#include "listComp.h"
//=============================================================================================
//temp -- will refactor later:
//-verbosity 5 -ontheflystopat 25 -qlog ../QueryLog/uniqTerms_from1k -morethankterms 0 -printallres 2
void quantizeAndSave(std::vector<std::string>& terms, std::vector<NewIteratorAugmented*>& lists ) {
	const std::string rootPath = "/research/index/globalLinearQuant/";
	SqlProxy sql(rootPath+"indx.sqlite");
//	const score_t minS = 0.000759046f;
	const score_t maxS = 70.903f;
	const score_t globalQuantile = maxS/256.0f;
	ListCompression::makeDirs(rootPath);
	for(unsigned l=0; l<lists.size(); ++l) {
		NewIteratorAugmented* list = lists[l];
		const unsigned sz = list->getPaddedLength();
		vecUInt dids;
		dids.resize(sz);
		std::vector<score_t> scores;
		scores.resize(sz);
		for(unsigned bl=0; bl<sz/CONSTS::NUMERIC::BS; ++bl)
			list->entireBlockDidsAndScores((bl*64) + &dids[0], (bl*64)+&scores[0],bl);
		vecUInt  freqs;
		freqs.resize(sz);
		for(unsigned i=0; i<sz; ++i)
			freqs[i] = scores[i]/globalQuantile;
		float maxScore = *(std::max_element(scores.begin(),scores.end()));
		ListCompression cmprsr(terms[l]);
		cmprsr.compressDocsAndFreqs(dids,freqs,maxScore,true);
		cmprsr.serializeToFS(rootPath);
        	cmprsr.serializeToDb(rootPath, sql);

	}
}

PriorityArray<QpResult<score_t> > Executor::runAlgorithm(std::vector<std::string>& terms, std::vector<NewIteratorAugmented*>& lists, score_t threshold) {
//	quantizeAndSave(terms,lists);

	OR Or(lists,options,threshold);
	//COUT << terms << " " << lists[0]->getPaddedLength() << " ";
	profiler.start(enm::COMPLETEQP);
	//PriorityArray<QpResult>  res(test.doExhaustiveScan(options.TOPK));
	PriorityArray<QpResult<score_t> >  res(Or.BMM(options.TOPK)); //exhaustive: 103,WAND: 43,Maxscore: 20,BMW: err,BMM: err
	//PriorityArray<QpResult<score_t> > res;
	//OR.LAandPostingsDistributions(options.TOPK);

	COUT << profiler.end(enm::COMPLETEQP)/1000.0 << " ms\n";

	return res;
}
//=============================================================================================
PriorityArray<QpResult<score_t> > Executor::runAlgorithm(std::vector<std::string>& terms, std::vector<NewIteratorIntAugmented*>& lists, scoreInt_t threshold) {
	OR Or(lists,options,threshold/CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE);
	COUTD << terms << " " << ENDL;
	profiler.start(enm::COMPLETEQP);
	//PriorityArray<QpResult>  res(test.doExhaustiveScan(options.TOPK));
	PriorityArray<QpResult<score_t> >  res(Or.BMWInt(options.TOPK));
//	PriorityArray<QpResult>  res;
	profiler.end(enm::COMPLETEQP);

	return res;
}
//=============================================================================================
void Executor::run(){
	QueryTraceManager logManager(queryLog,&index.lex);
	logManager.loadScoresFromFile(options.GOLDENSCORES.c_str());
	QueryTraceManager::queriesFileIterator queriesFIterator(logManager);
	queriesFIterator.changeSkipPolicy(options.LIMIT,options.KTERMSBUCKET,options.MORETHANKTERMS);
	unsigned queryNum = 0;
	while( queryNum++ <options.LIMIT) {

		if(queryNum<options.SKIPTOQUERY) {
			++queriesFIterator;
			//++queriesFIteratorThreshold;
			continue;
		}

		++queriesFIterator;
		//++queriesFIteratorThreshold;
		if(queriesFIterator == logManager.end())
			break;

		std::vector<std::string> queryTerms = (*queriesFIterator);
		LOG << queryTerms << ENDL;
		COUT4 << queryTerms << ENDL;

		//inject new iterators
		auto lps = index[queryTerms];
		std::vector<NewIteratorAugmented*> lists;
		lists.reserve(lps.size());
		for(unsigned i=0; i<lps.size(); ++i) {
			lists.push_back(NewIteratorAugmented::scratchPads+i);
			lists[i]->setScratchId(i);
			lists[i]->maxScoreOfList = lps[i]->maxScoreOfList;
			lists[i]->initBM25Float(lps[i]->unpaddedLength);
			lists[i]->injectCompressedState( lps[i]->unpaddedLength,
					lps[i]->compressedData.getMaterializedData(),
					&lps[i]->sizePerBlock[0], &lps[i]->maxDidPerBlock[0], &lps[i]->flags[0] );
			lists[i]->setPolicy(womanly.suggestPolicy(lps[i]->term));
			lists[i]->setUseReals(lists[i]->getPolicy() == enm::ONTHEFLY);
			lists[i]->initListFromCache(womanly.getTheGoodStuff(lps[i]->term));
			lists[i]->initListFromCacheFloatPart(womanly.getTheGoodStuff(lps[i]->term));
			
		}

		score_t threshold = (options.CLRVTPERCENT/100.0)*queriesFIterator.score();
		PriorityArray<QpResult<score_t> > resultsHeap = runAlgorithm(queryTerms,lists,threshold);

		if(options.PRINTALLRES>1)
			continue;

		QpResult<> res[options.TOPK];
		resultsHeap.putInArray(res);
		//releaseStructs(lps);

		// score fix
		int pos = options.TOPK-1;
		//while (Fcompare(0.0f, res[pos].score) == 0 ) { pos--; }
		float score = res[pos].score; //res[options.TOPK-1].score; //resLogger(qn, word_l, res, topk);

		//debug write integer golden scores
//		FILE * gThresholdIntFile = fopen ("Golden_Threshold_trec05_updated","a"); //hardcoded here, todo move to globals.h
//		if (gThresholdIntFile!=NULL) {
//			for(size_t i=0; i<queryTerms.size(); ++i) {
//				fprintf(gThresholdIntFile, "%s", queryTerms[i].c_str());
//				fprintf(gThresholdIntFile, " ");
//			}
//			fprintf(gThresholdIntFile, "%f", score);
//			fprintf(gThresholdIntFile, "\n");
//		}
//	    fclose (gThresholdIntFile);

		if(options.PRINTALLRES) {
			COUT << queryTerms << ENDL;
			for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
				COUT << res[i].did << " " << res[i].score << ENDL;
		}
		// Sanity check, if top-kth score matches with the precomputed golden threshold
		else if (!FloatEquality(score, queriesFIterator.score())) {
			CERR << "golden score: " <<   queriesFIterator.score() << "!=" << score << ENDL;

			for(size_t i=0; i<queryTerms.size(); ++i)
				CERR << queryTerms[i] << ",";
			CERR << ENDL;

			for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
				CERR << i << "\t" << res[i].did << "\t" << res[i].score << ENDL;
		}
	} // end of qp while loop
}
//=============================================================================================
void Executor::runInt(){
	QueryTraceManager logManager(queryLog,&index.lex);
	logManager.loadScoresFromFile(options.GOLDENSCORES.c_str());
	QueryTraceManager::queriesFileIterator queriesFIterator(logManager);
	queriesFIterator.changeSkipPolicy(options.LIMIT,options.KTERMSBUCKET,options.MORETHANKTERMS);
	unsigned queryNum = 0;
	while( queryNum++ <options.LIMIT) {

		if(queryNum<options.SKIPTOQUERY) {
			++queriesFIterator;
			//++queriesFIteratorThreshold;
			continue;
		}

		++queriesFIterator;
		//++queriesFIteratorThreshold;
		if(queriesFIterator == logManager.end())
			break;

		std::vector<std::string> queryTerms = (*queriesFIterator);
		LOG << queryTerms << ENDL;
		COUT4 << queryTerms << ENDL;

		//inject new iterators
		auto lps = index[queryTerms];
		std::vector<NewIteratorIntAugmented*> lists;
		lists.reserve(lps.size());
		for(unsigned i=0; i<lps.size(); ++i) {
			lists.push_back(NewIteratorIntAugmented::scratchPads+i);
			lists[i]->setScratchId(i);
			lists[i]->maxScoreOfList = lps[i]->maxScoreOfList/CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE; //todo: quantize
			lists[i]->injectCompressedState( lps[i]->unpaddedLength,
					lps[i]->compressedData.getMaterializedData(),
					&lps[i]->sizePerBlock[0], &lps[i]->maxDidPerBlock[0], &lps[i]->flags[0] );
			lists[i]->setPolicy(womanly.suggestPolicy(lps[i]->term));
			lists[i]->setUseReals(lists[i]->getPolicy() == enm::ONTHEFLY);
			lists[i]->initListFromCache(womanly.getTheGoodStuff(lps[i]->term));
			lists[i]->initListFromCacheIntPart(womanly.getTheGoodStuff(lps[i]->term));
			
		}

		scoreInt_t threshold = (options.CLRVTPERCENT/100.0)*queriesFIterator.score();
		PriorityArray<QpResult<> > resultsHeap = runAlgorithm(queryTerms,lists,threshold);

		if(options.PRINTALLRES>2)
			continue;

		QpResult<> res[options.TOPK];
		resultsHeap.putInArray(res);
		//releaseStructs(lps);
		
		if(options.PRINTALLRES == 2) {
			score_t minS = res[0].score;
			for(unsigned i=0; i<options.TOPK; ++i)
				if(minS > res[i].score && res[i].score > 0.001f)
					minS = res[i].score;
			COUT << queryTerms << std::setprecision(16) << minS << ENDL;
			continue;
		}
		float score = res[options.TOPK-1].score; //resLogger(qn, word_l, res, topk);

//		//TOGO and TODO
//		FILE * gThresholdIntFile = fopen ("Golden_Threshold_trec06_INT","a"); //hardcoded here, todo move to globals.h
//		if (gThresholdIntFile!=NULL) {
//			for(size_t i=0; i<queryTerms.size(); ++i) {
//				fprintf(gThresholdIntFile, "%s", queryTerms[i].c_str());
//				fprintf(gThresholdIntFile, " ");
//			}
//			fprintf(gThresholdIntFile, "%f", score);
//			fprintf(gThresholdIntFile, "\n");
//		}
//	    fclose (gThresholdIntFile);

		if(options.PRINTALLRES) {
			COUT << queryTerms << ENDL;
			for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
				COUT << res[i].did << " " << res[i].score << ENDL;
		}
		// Sanity check, if top-kth score matches with the precomputed golden threshold
		else if (!FloatEquality(score, queriesFIterator.score())) {
			CERR << "golden score: " <<   queriesFIterator.score() << "!=" << score << ENDL;

			for(size_t i=0; i<queryTerms.size(); ++i)
				CERR << queryTerms[i] << ",";
			CERR << ENDL;

			for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
				CERR << i << "\t" << res[i].did << "\t" << res[i].score << ENDL;
		}
	} // end of qp while loop
}
//=============================================================================================
void Executor::printReport() const {  profiler.printReport(); }
//=============================================================================================
bool QueryTraceManager::queriesFileIterator::isSkip() const {
	//return false;
	if(limit && count>=limit)
		return true;
	const size_t sz = queriesP[curr].size();
	if(sz == 0) {
		CERR << "empty query in log: " << curr << ENDL;
		return true;
	}
	if(sz<=moreThanKTerms) return true;
	if(sz>10) return true;
	return exactlyKTermsBucket > 0 ? sz != exactlyKTermsBucket : false;
}
//=============================================================================================
QueryTraceManager::queriesFileIterator& QueryTraceManager::queriesFileIterator::operator++() {
	++curr;
	while((unsigned)curr < queriesP.size() && isSkip()) {
		++curr;
	}
	++count;
	return *this;
}
//=============================================================================================
static std::string joinStringsVector(const std::vector<std::string>& terms) {
	std::stringstream buffer;
	std::copy(terms.begin(), terms.end(), std::ostream_iterator<std::string>( buffer ) );
	return buffer.str();
}
//=============================================================================================
float QueryTraceManager::score(const std::vector<std::string>& terms) const {
	const std::string joined(joinStringsVector(terms));
	strToFloatMap::const_iterator it = mapQueryToScore.find(joined);
	if( it == mapQueryToScore.end()) FATAL("couldn't find: " + joined);

	return  (*it).second;
}
//=============================================================================================
void QueryTraceManager::setScoreForQuery(const std::vector<std::string>& terms, float v) {
	mapQueryToScore[joinStringsVector(terms)] = v;
}
//=============================================================================================
void QueryTraceManager::loadScoresFromFile(const char* fname) {
	FILE *fq = fopen(fname,"r");
	std::vector<std::string> terms;
	while(1)  {
		terms.clear();
		char line[5120];

		if( readline(fq, line) == -1 ) //this is the end
			break;

		std::string token, text(line);
		std::istringstream iss(text);
		while ( getline(iss, token, ' ') )
			terms.push_back(token);

		//well, last one is not a term, but a score
		float score = atof(terms[terms.size()-1].c_str());
		terms.pop_back();
		setScoreForQuery(terms,score);
	}
	fclose(fq);
}
//=============================================================================================
QueryTraceManager::QueryTraceManager(const std::string& fname, termsMap *l) :lex(l){
	assert(l->size());

	FILE *fq = fopen(fname.c_str(),"r");
	queriesD.reserve(10000);
	std::vector<std::string> terms;
	while(1)  {
		terms.clear();
		char line[5120];

		if( readline(fq, line) == -1 ) //this is the end
			break;

		char *word;
		word = strtok(line," ");

		if( lex->find(std::string(word))!= lex->end() ) //only if term is in cache...
			terms.push_back(word);
		else
			LOG << word << " is not in lex" << ENDL;

		while((word = strtok(NULL," ")) != NULL){
			if( lex->find(std::string(word))!= lex->end() ) //only if term is in cache...
				terms.push_back(word);
			else
				LOG << word << " is not in lex" << ENDL;
		}

		queriesD.push_back(terms);
		setScoreForQuery(terms,0.0);
	}
	//COUT4 << queriesD.size() << " queries loaded" << ENDL;
	fclose(fq);
}
//=============================================================================================
void PreprocessingOracleManager::loadPoliciesFromFile(const std::string& fname) {
	if(fname == "") return;
	std::fstream is(fname.c_str());
	std::string term, policyStr;
	enm::prprPolicy policy;
	while(is.good()) {
		is >> term >> policyStr;
		convert(policyStr,policy);
		if(storedPolicies.count(term) && storedPolicies[term]!=policy) 
			COUT4 << "Overriding " <<  storedPolicies[term] << " with " << policy << " in " << term << ENDL;
		
		storedPolicies[term]=policy;
	}
}
//=============================================================================================
void PreprocessingOracleManager::setStaticPolicy(const listIndexMetaData& list, const unsigned ontheflystopat) {
	const std::string& term = list.term;
	const unsigned lengthOfList = list.unpaddedLength;

	// Cost-based - Policy to override the length-based (OTF policy for non-set terms)
	if(lengthOfList < (1<<25) )
		storedPolicies[term]=enm::ONTHEFLY;

	// Policies manually set such that their BM (only) size be from 0.5, ... 6GBs
	//	Length-based setup when LB without PBs are used
	//	OTF stop 	BMQC stop	size (GBs)
	//	873633	873633	0.5
	//	457707	457707	1
	//	95042	262144	2
	//	26536	262144	3
	//	20354	262144	4
	//	16394	262144	5
	//	13374	262144	6
//	if(lengthOfList < 95042 ) // 2gb setup
//		storedPolicies[term]=enm::ONTHEFLY;
//	else if(lengthOfList < 262144)
//		storedPolicies[term]=enm::BMQC_FAKE;
//	else
//		storedPolicies[term]=enm::BMQ_FAKE;

	// LB with PB setup 4GB
//	if(lengthOfList < 179758 ) //(1<<ontheflystopat))
//		storedPolicies[term]=enm::ONTHEFLY;
//	else if(lengthOfList < 179758 )//(1<<18))
//		storedPolicies[term]=enm::BMQC_PBS; // cmp
//	else if(lengthOfList < (1<<22))
//		storedPolicies[term]=enm::BMQ_PBS;
//	else
//		storedPolicies[term]=enm::BMQ_FAKE;

	//test: override
	//storedPolicies[term]=enm::BMQC_PBS;
	//COUT << term << " " << storedPolicies[term] << ENDL;
}
//=============================================================================================
enm::prprPolicy PreprocessingOracleManager::suggestPolicy(const std::string& term) {
	assert(storedPolicies.count(term));
	return storedPolicies[term];
}
//=============================================================================================
PreprocessingData& PreprocessingOracleManager::getTheGoodStuff(const std::string& term) {
#ifdef DEBUG
	assert(storedPolicies.count(term));
	if(storedPolicies[term] != enm::ONTHEFLY) {
		assert(storedPreprocessedAugments.count(term));
	}
#endif
	return storedPreprocessedAugments[term];
}
//=============================================================================================
void PreprocessingOracleManager::preloadAndStoreAccordingToPolicy(std::vector<listIndexMetaData*>& lists) {
	for(listIndexMetaData* list : lists){
		if(storedPreprocessedAugments.count(list->term)) continue;//we have it already

		#ifdef USEINTPIPE
			NewIteratorIntAugmented& tmp = NewIteratorIntAugmented::scratchPads[0];
		#else
			NewIteratorAugmented& tmp = NewIteratorAugmented::scratchPads[0];
		#endif
		tmp.setPolicy(suggestPolicy(list->term));
		tmp.injectCompressedState( list->unpaddedLength,
				list->compressedData.getMaterializedData(),
				&list->sizePerBlock[0], &list->maxDidPerBlock[0], &list->flags[0] );
		#ifndef USEINTPIPE
			tmp.initBM25Float(list->unpaddedLength);		
		#endif

		COUT4 << "build " << list->term << " ["  << list->unpaddedLength << "] with policy: " << tmp.getPolicy() << ENDL;
		storedPreprocessedAugments.insert({list->term, tmp.buildInitialAugnemtsData()});
	}
}
//=============================================================================================
Executor::Executor(Options& opts,const std::string& queryLogF)
:options(opts), profiler(profilerC::getTheInstance()),queryLog(queryLogF){
	NewIteratorBase::setDocLenPtr(index.getDocLensPtr());
	//pre-loading of on-demands to memory:
	QueryTraceManager logManager(queryLog,&index.lex);
	QueryTraceManager::queriesFileIterator queriesFIterator(logManager);
	queriesFIterator.changeSkipPolicy(options.LIMIT,options.KTERMSBUCKET,options.MORETHANKTERMS);
	NewIteratorAugmented::initScratchpads();
	NewIteratorIntAugmented::initScratchpads();

	unsigned qn=0;
	while(true) { //go over all queries and preload them
		if(qn<options.SKIPTOQUERY) {
			++queriesFIterator;
			continue;
		}

		++queriesFIterator;
		if(queriesFIterator == logManager.end())
			break;

		std::vector<std::string> terms = (*queriesFIterator);
		COUTD << "preloading " << terms << ENDL;
		std::vector<listIndexMetaData*> lists = index[terms];

		//set static policy for list. will be overridden by stored policies if present
		for(listIndexMetaData* list : lists)
			womanly.setStaticPolicy(*list,options.ONTHEFLYSTOPAT);

		womanly.loadPoliciesFromFile(options.POLICY);
		//COUT << terms << lists[0]->unpaddedLength << " ";
		womanly.preloadAndStoreAccordingToPolicy(lists); //preloads all

	}

	options.print(LOG);
	if(options.VERBOSITY>2)
		options.print(COUT);

}
