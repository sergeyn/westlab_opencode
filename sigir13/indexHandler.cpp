#include "indexHandler.h"
#include "SqlProxy.h"
#include "utils.h"

static unsigned* tempStore(nullptr);
size_t onDemandCPool::counter;
size_t onDemandCPool::capacity;
std::vector<onDemandCPool*> onDemandCPool::storage;
termsMap onDemandCPool::termsToInd;
//=============================================================================================
void onDemandCPool::evictData() {
	data.clear();
	data = vecUInt(); //clearing and resizing was causing mem. leaks on my system - hence this
	COUTD << "Note: evicting list " << termOrPath << ENDL;
}
//=============================================================================================
void onDemandCPool::initStaticPool(const unsigned cap) {
	counter=0;
	capacity=cap;
	storage.clear();
	storage.resize(cap,0);
}
//=============================================================================================
template <typename T>
void getBlob(sqlite3_stmt* ppStmt, int cid, std::vector<T>& v) {
	if(sqlite3_column_type(ppStmt, cid) != SQLITE_BLOB)
		return;
	const int size = sqlite3_column_bytes(ppStmt, cid) / sizeof(T);
	const T* src = (const T*) sqlite3_column_blob(ppStmt, cid);
	v.clear();
	v.reserve(size);
	for(int i=0; i<size; ++i)
		v.push_back(src[i]);
}
//=============================================================================================
template<typename ListStruct>
std::vector<ListStruct> loadIndexMeta(const std::string& where=""){
	SqlProxy ld;
	sqlite3_stmt* ppStmt = ld.prepare("select * from terms "+where); //tODO: make iterator
	if(! ppStmt) FATAL("select failed");

	std::vector<ListStruct> centry;
	centry.reserve(1<<14);

	//'/flag/','/max/','/min/','/score/','/size/'
	while (sqlite3_step(ppStmt) == SQLITE_ROW) {  // For each row returned
		ListStruct c;
		c.term = std::string((const char*)sqlite3_column_text(ppStmt, 0));
		c.compressedData.setName(CONSTS::INDEX::TERM_PATH + "/" + c.term);
		c.metaInfoLength = sqlite3_column_bytes(ppStmt, 1) / sizeof(unsigned int);
		getBlob(ppStmt,1,c.flags);
		getBlob(ppStmt,2,c.maxDidPerBlock);
		getBlob(ppStmt,4,c.DEPmaxScorePerBlock);
		getBlob(ppStmt,5,c.sizePerBlock);
		c.maxScoreOfList = *(std::max_element(c.DEPmaxScorePerBlock.begin(),c.DEPmaxScorePerBlock.end()));
		centry.push_back(c);
	} //end while
	sqlite3_finalize(ppStmt);
	return centry;
}
//=============================================================================================
static unsigned int readEntireFileToTmpBuffer(const char* fname, unsigned int* buffer){
	FILE* fin = fopen(fname,"r");
	if(fin == NULL) FATAL("couldn't open " +std::string(fname));

	unsigned int readInts = 0;
	while( fread(&(buffer[readInts]), sizeof(unsigned int), 1, fin) == 1 )
		++readInts;

	fclose(fin);
	return readInts;
}
//=============================================================================================
inline bool isCollision(const std::string& victim, const std::vector<std::string>& peers) {
	return peers.end() != std::find(peers.begin(), peers.end(),victim);
}
//=============================================================================================
unsigned onDemandCPool::materialize( const std::vector<std::string>& peers) {
	//test if hit
	termsMap::const_iterator ind = termsToInd.find(termOrPath);

	if(ind != termsToInd.end() && (*ind).second >= 0)
		return data.size(); //this one is materialized already

	//no hit, so we need to place it in storageCOUT4<< "Loaded: " << data.size() << " index info items" << ENDL;
	while(storage[counter] && isCollision(storage[counter]->termOrPath,peers)) //pick non-colliding victim for eviction
		counter = (counter+1)%capacity;

	if(storage[counter]) { //the bucket is occupied
		storage[counter]->evictData();
		termsToInd[storage[counter]->termOrPath]=-1; //evict
	} // when query has duplicate terms this scheme should still work

	termsToInd[termOrPath]=counter;
	storage[counter]=this; //binding to this guy
	counter = (counter+1)%capacity;

	//now actually get the data, update the buffer pointer and the size
	if(nullptr == tempStore) tempStore = NEWM unsigned[CONSTS::NUMERIC::MAXD];
	size_t size = readEntireFileToTmpBuffer(termOrPath.c_str(),tempStore);
	data = vecUInt(tempStore,tempStore+size);
	return size;
}
//=============================================================================================
void termsIndexCache::addSingleToCache(const std::string& term) {
	std::vector<listIndexMetaData> arr = loadIndexMeta<listIndexMetaData>("where term='"+term+"'");
	data.push_back(arr[0]);
}
//=============================================================================================
void termsIndexCache::fillCache(const termsMap& lex){
	onDemandCPool::initStaticPool(CONSTS::INDEX::STORAGE_CAPACITY);
	data = loadIndexMeta<listIndexMetaData>();

	for(listIndexMetaData& list : data) {
		list.maxDidPerBlock.insert(list.maxDidPerBlock.begin(),0); //push 0 to make decompression sensible
		list.maxDidPerBlock.push_back(CONSTS::NUMERIC::MAXD+256); //push MAXD to make decompression sensible

		list.unpaddedLength = lex.find(list.term)->second; //set the unpadded length from lexicon
		//prefix sum sizes
		//list.flags.insert(list.flags.begin(),list.flags.front()); //push to make decompression sensible
		list.sizePerBlock.insert(list.sizePerBlock.begin(),0); //push 0 to make decompression sensible
		for( unsigned i=1; i<list.sizePerBlock.size(); ++i)
			list.sizePerBlock[i] += list.sizePerBlock[i-1] +1; // +1 ? Shuai!!!
	}

	COUT4<< "Loaded: " << data.size() << " index info items" << ENDL;
}
//=============================================================================================
listIndexMetaData& termsIndexCache::operator[](size_t i){
	assert(i<data.size());
	listIndexMetaData& d = data[i];
	d.compressedDataLength = d.compressedData.materialize(std::vector<std::string>());
	return data[i];
}
//=============================================================================================
//void termsIndexCache::materializeAll() {
//	for(listIndexMetaData list : data)
//		list.compressedDataLength = list.compressedData.materialize(std::vector<std::string>());
//}
//=============================================================================================
typedef std::pair<std::vector<std::string>, std::vector<size_t> > stringIntVectorsPair;

static stringIntVectorsPair getTrecWordsMappingList(FILE* ftrec) {
	std::vector<std::string> terms;
	std::vector<size_t> ids;
	int wid;
	char word[1024];
	while(fscanf(ftrec,"%s\t%d\n",word, &wid) != EOF ) {
		terms.push_back(std::string(word));
		ids.push_back(wid);
	}
	COUT4 << "Loaded: " << terms.size() << " term mappings" << ENDL;
	return stringIntVectorsPair(terms,ids);
}
//=============================================================================================
static termsMap getTrecWordsMappingMap(const std::string& qMappingPath){
	FILE* ftrec = fopen(qMappingPath.c_str(),"r");
	if(! ftrec) FATAL("could not load: " + qMappingPath );
	termsMap t;
	const stringIntVectorsPair& p = getTrecWordsMappingList(ftrec);
	fclose(ftrec);
	for(size_t i=0; i<p.first.size(); ++i)
		t[p.first[i]] = p.second[i];
	return t;
}
//=============================================================================================
indexHandler::indexHandler() {
	fillTermsMap();
	Cache.fillCache(lex);
	for(size_t i=0; i<Cache.size(); ++i)
		termsInCache[std::string(Cache.getTerm(i))]=i;
	mappingForOnDemand = getTrecWordsMappingMap(CONSTS::INDEX::zeroMapping);
	loadDocLengths();
}
//=============================================================================================
void indexHandler::loadDocLengths(const std::string& fname) {
	const unsigned docn = CONSTS::NUMERIC::MAXD;
	documentLength.resize(docn);

	FILE *fdoclength = fopen64(fname.c_str(),"r");
	if( fdoclength == NULL) FATAL(" doc length file is missing: " + fname);

	if( fread(&documentLength[0],sizeof(unsigned), docn, fdoclength) != docn )
		FATAL("wrong doc length when processing " + fname);

	fclose(fdoclength);

	for(int i =0;i<128 ; i++)
		documentLength[docn + i] = docn/2; //why this black magic?
}
//=============================================================================================
void indexHandler::fillTermsMap(const std::string& path) {
	FILE* flex = fopen(path.c_str(),"r");
	if(flex==NULL) 	FATAL(path+" could not be opened");
	char term[1024];
	int length;
	while( fscanf(flex,"%s\t%d\n",term, &length)!= EOF )
		lex[std::string(term)] = length;
	fclose(flex);
	assert(lex.size());
	COUT4 << "Loaded: " << lex.size() << " terms to lexicon mapping" << ENDL;
}
//=============================================================================================
std::vector<listIndexMetaData*> indexHandler::operator[] (const std::vector<std::string>& terms){
	// create structure and reserve resources
	std::vector<listIndexMetaData*> lps;
	lps.reserve(terms.size());
	//prepare peers
	std::vector<std::string> peers( terms.size());

	for(size_t i=0; i<terms.size(); i++) {
		if(termsInCache.find(terms[i]) == termsInCache.end()) //no term!
			FATAL("lacking term: " + terms[i]);
		peers.push_back(Cache[ termsInCache[terms[i]]].compressedData.getName() );
	}

	// note: this one might break when query has duplicate terms -- but this is wrong anyway :)
	for(size_t i=0; i<terms.size(); i++)	{
		listIndexMetaData* lpsi = &(Cache[termsInCache[terms[i]]]);
		lps.push_back(lpsi);

#ifdef USEQUANTINDEX
//		//load lookup table
//		ifstream lookupF((CONSTS::trecRoot + "quant/" + word_l[i]).c_str());
//		float val;
//		unsigned q=0;
//		while(lookupF.good()) {
//			lookupF >> val;
//			lpsi->lookup[q++]=val;
//		}
#endif
	}
	return lps;
}
